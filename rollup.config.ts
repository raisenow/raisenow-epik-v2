/* eslint "node/no-unpublished-import": 0 */
import {config as dotenvConfig} from 'dotenv'
import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import json from '@rollup/plugin-json'
import babel from '@rollup/plugin-babel'
import {terser} from 'rollup-plugin-terser'
import visualizer from 'rollup-plugin-visualizer'

///////////////////////////////////////////////////////////////////////////////

dotenvConfig({path: './.env'})
process.env.NODE_ENV = 'production' // this may be needed for proper minification of some libs

///////////////////////////////////////////////////////////////////////////////

const stats = false
const debug = false

///////////////////////////////////////////////////////////////////////////////

// eslint-disable-next-line @typescript-eslint/no-var-requires
const {unpkg, main, module} = require('./package.json')
const input = './src/index.ts'
const name = 'rnw.epik'
const extensions = ['.js', '.ts', '.json']

///////////////////////////////////////////////////////////////////////////////

const external = (id) => /^(?!\.{0,2}\/)/.test(id)

const onwarn = (warning, warn) => {
  if (
    !debug &&
    [
      'CIRCULAR_DEPENDENCY',
      'NON_EXISTENT_EXPORT',
      'UNUSED_EXTERNAL_IMPORT',
    ].includes(warning.code)
  ) {
    return
  }

  warn(warning)
}

///////////////////////////////////////////////////////////////////////////////

const browser = {
  input,
  output: {
    file: unpkg,
    format: 'iife',
    name,
    sourcemap: true,
  },
  onwarn,
  plugins: [
    resolve({
      extensions,
      mainFields: ['browser', 'module', 'jsnext:main', 'main'],
    }),
    commonjs(),
    json(),
    babel({
      babelrc: false,
      configFile: './babel.config.browser.js',
      extensions,
      babelHelpers: 'runtime',
      skipPreflightCheck: true,
    }),
    terser(),
    stats &&
      visualizer({
        filename: 'dist/stats.browser.html',
        template: 'treemap',
        open: true,
      }),
  ].filter(Boolean),
}

const commonJs = {
  input,
  output: {
    file: main,
    format: 'cjs',
    sourcemap: true,
  },
  external,
  onwarn,
  plugins: [
    resolve({
      extensions,
      mainFields: ['module', 'jsnext:main', 'main'],
    }),
    commonjs(),
    json(),
    babel({
      babelrc: false,
      configFile: './babel.config.node.js',
      extensions,
      babelHelpers: 'runtime',
    }),
    stats &&
      visualizer({
        filename: 'dist/stats.cjs.html',
        template: 'treemap',
        open: true,
      }),
  ].filter(Boolean),
}

const es6module = {
  input,
  output: {
    file: module,
    format: 'es',
    sourcemap: true,
  },
  external,
  onwarn,
  plugins: [
    resolve({
      extensions,
      mainFields: ['module', 'jsnext:main', 'main'],
    }),
    commonjs(),
    json(),
    babel({
      babelrc: false,
      configFile: './babel.config.node.js',
      extensions,
      babelHelpers: 'runtime',
    }),
    stats &&
      visualizer({
        filename: 'dist/stats.esm.html',
        template: 'treemap',
        open: true,
      }),
  ].filter(Boolean),
}

///////////////////////////////////////////////////////////////////////////////

export default () => {
  if (process.env.FORMAT === 'es6') {
    return [es6module]
  }

  return [browser, commonJs, es6module]
}
