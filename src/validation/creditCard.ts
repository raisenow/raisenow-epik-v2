import {getCreditCardType, PaymentMethod} from '../lib/PaymentMethod'
import {
  validateLengthBetween,
  validateNumberBetween,
  validateNumerical,
} from './generic'

///////////////////////////////////////////////////////////////////////////////

export const validateCreditCardNumber = (cardNumber: string): boolean => {
  return (
    validateNumerical(cardNumber) &&
    validateCreditCardLength(cardNumber) &&
    validateCreditCardLuhn(cardNumber)
  )
}

export const validateCreditCardLength = (cardNumber: string): boolean => {
  const type = getCreditCardType(cardNumber)

  if (type === PaymentMethod.AMERICAN_EXPRESS) {
    return [15].includes(cardNumber.length)
  }
  if (type === PaymentMethod.VISA) {
    return [13, 16, 19].includes(cardNumber.length)
  }
  if (type === PaymentMethod.MASTER_CARD) {
    return [16].includes(cardNumber.length)
  }
  if (type === PaymentMethod.DISCOVERY) {
    return [16, 17, 18, 19].includes(cardNumber.length)
  }
  if (type === PaymentMethod.DINERS_CLUB) {
    return [14, 15, 16, 17, 18, 19].includes(cardNumber.length)
  }
  if (type === PaymentMethod.JAPANESE_CREDIT_BUREAU) {
    return [16, 17, 18, 19].includes(cardNumber.length)
  }
  if (type === PaymentMethod.MAESTRO) {
    return [12, 13, 14, 15, 16, 17, 18, 19].includes(cardNumber.length)
  }

  return false
}

export const validateCreditCardLuhn = (cardNumber: string): boolean => {
  let s = 0
  let doubleDigit = false

  for (let i = cardNumber.length - 1; i >= 0; i--) {
    let digit = +cardNumber[i]
    if (doubleDigit) {
      digit *= 2
      if (digit > 9) {
        digit -= 9
      }
    }
    s += digit
    doubleDigit = !doubleDigit
  }

  return s % 10 === 0
}

export const validateCreditCardCvv = (input: string): boolean => {
  return validateNumerical(input) && validateLengthBetween(input, 3, 4)
}

export const validateCreditCardExpirationMonth = (input: string): boolean => {
  return validateNumberBetween(input, 1, 12)
}

export const validateCreditCardExpirationYear = (input: string): boolean => {
  let expy = parseInt(input)
  expy = expy < 100 ? expy + 2000 : expy

  const currentYear = new Date().getFullYear()

  return validateNumberBetween(expy, currentYear, currentYear + 50)
}
