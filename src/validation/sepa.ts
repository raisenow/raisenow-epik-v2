// This file categorises countries based on the IBAN. Some territories have
// their own ISO code, but are using the code of the country they belong to.
// For example, Jersey (JE) uses GB in their IBAN.

export const eeaCountries = [
  'AT',
  'BE',
  'BG',
  'CY',
  'CZ',
  'DE',
  'DK',
  'EE',
  'ES',
  'FI',
  'FR',
  'GR',
  'HR',
  'HU',
  'IE',
  'IS',
  'IT',
  'LI',
  'LT',
  'LU',
  'LV',
  'MT',
  'NL',
  'NO',
  'PL',
  'PT',
  'RO',
  'SE',
  'SI',
  'SK',
]

export const sepaCountries = [
  ...eeaCountries,
  'CH',
  'GB',
  'GI',
  'MC',
  'SM',
  'VA',
]

// These French overseas collectivities are non-EEA territories but use FR
// for their IBAN:
//   • Saint Barthélemy (BL)
//   • Saint Pierre and Miquelon (PM)
//   • Wallis and Futuna (WF)
// For them, we allegedly need the BIC. To be on the safe side, it was
// specified to show the BIC field for all IBAN with country code FR.
export const isBicFieldRequired = (country?: string | null): boolean =>
  (sepaCountries.includes(country) && !eeaCountries.includes(country)) ||
  country === 'FR'
