export const validateMinLength = (input: string, length: number): boolean => {
  return input && input.length >= length
}

export const validateMaxLength = (input: string, length: number): boolean => {
  return input && input.length <= length
}

export const validateLengthBetween = (
  input: string,
  minLength: number,
  maxLength: number,
): boolean => {
  return (
    validateMinLength(input, minLength) && validateMaxLength(input, maxLength)
  )
}

export const validateNumerical = (input: string | number): boolean => {
  return /^[.\d]+$/.test(input as string)
}

export const validateNumberBetween = (
  input: number | string,
  min: number,
  max: number,
): boolean => {
  const number = parseFloat(input as string)

  return number >= min && number <= max
}
