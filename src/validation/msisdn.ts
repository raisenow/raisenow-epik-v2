export const validateMsisdn = (input: string): boolean => {
  const format = /^\+417[56789][0-9]{7}$/

  return format.test(input)
}
