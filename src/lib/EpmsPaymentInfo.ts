import {EpmsPaymentInfoBase} from './EpmsPaymentInfoBase'
import {EpmsSupporterInfo} from './EpmsSupporterInfo'

///////////////////////////////////////////////////////////////////////////////

export type EpmsPaymentInfo = EpmsPaymentInfoBase & {
  provider_payment_id: string | null
  external_payment_id: string | null

  created_payment_source_uuid: string | null
  charged_payment_source_uuid: string | null
  created_subscription_uuid: string | null
  charged_subscription_uuid: string | null
  charged_by: string | null

  amount: number
  currency_identifier: string // PaymentCurrency

  refunds: any[]
  cancellations: any[]

  supporter_snapshot: EpmsSupporterInfo

  has_refunds: boolean
  has_cancellations: boolean
}
