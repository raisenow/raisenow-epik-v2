import {each, isArray, isPlainObject, isString, map, reduce} from 'lodash'
import {PaymentConfig} from '../PaymentConfig'

////////////////////////////////////////////////////////////////////////////////

export const findDefinedValue = (...values: any[]): any =>
  values.find((v) => v !== null && v !== undefined && v !== '')

///////////////////////////////////////////////////////////////////////////////

export const normalizeValues = (data: any): any => {
  if (isString(data)) {
    try {
      data = decodeURIComponent(data)
    } catch (error) {
      // do nothing
    }

    if (data.toLowerCase() === 'null') {
      return null
    }

    if (data.toLowerCase() === 'true') {
      return true
    }

    if (data.toLowerCase() === 'false') {
      return false
    }

    if (data.toLowerCase() === 'undefined') {
      return undefined
    }
  }

  if (isArray(data)) {
    return map(data, (v) => normalizeValues(v))
  }

  if (isPlainObject(data)) {
    return reduce(
      data,
      (res, v, k) => {
        res[k] = normalizeValues(v)

        return res
      },
      {},
    )
  }

  return data
}

///////////////////////////////////////////////////////////////////////////////

export const cleanBlankFields = (payment: PaymentConfig): void => {
  each(payment, (v, k) => {
    if (v === undefined || v === 'undefined' || v === null || v === 'null') {
      payment[k] = ''
    }
  })
}
