import qs from 'qs'
import {EpikQueryParam} from '../EpikQueryParam'

////////////////////////////////////////////////////////////////////////////////

export const prepareReturnUrl = (href: string): string => {
  const url = document.createElement('a')
  url.href = href

  const params = qs.parse(url.search, {ignoreQueryPrefix: true})

  delete params[EpikQueryParam.VIEW]
  delete params[EpikQueryParam.EPP_TRANSACTION_ID]
  delete params[EpikQueryParam.SUBSCRIPTION_TOKEN]
  delete params[EpikQueryParam.CUSTOMER_TOKEN]
  delete params[EpikQueryParam.EPMS_PAYMENT_UUID]
  delete params[EpikQueryParam.EPMS_SUBSCRIPTION_UUID]

  url.search = qs.stringify(params, {
    addQueryPrefix: true,
    strictNullHandling: true,
  })

  return url.href
}
