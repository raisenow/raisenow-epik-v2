import {PaymentFlow} from '../PaymentFlow'

////////////////////////////////////////////////////////////////////////////////

export const prepareExpirationYear = (
  input: string,
  flow: PaymentFlow,
): string => {
  let expy = parseInt(input)

  if (isNaN(expy)) {
    return input
  }

  // for epp - YY, for epms - YYYY
  expy = flow === PaymentFlow.EPP && expy > 2000 ? expy - 2000 : expy
  expy = flow === PaymentFlow.EPMS && expy < 2000 ? expy + 2000 : expy

  return expy.toString()
}

////////////////////////////////////////////////////////////////////////////////

export const getUsersTimeZone = () => {
  try {
    return Intl.DateTimeFormat().resolvedOptions().timeZone
  } catch (error) {
    return 'Europe/Zurich'
  }
}
