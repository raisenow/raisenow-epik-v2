import {each} from 'lodash'
import qs from 'qs'
import {Router, RouterLocation} from '../../router'

////////////////////////////////////////////////////////////////////////////////

export type LocationParams = {
  [key: string]: any
}

export type GetUpdatedLocationArgs = Partial<{
  pathname: string
  hash: string
  params: LocationParams
}>

///////////////////////////////////////////////////////////////////////////////

export const getLocationParams = (router: Router): LocationParams => {
  const {search} = router.getLocation()

  return qs.parse(search, {ignoreQueryPrefix: true})
}

///////////////////////////////////////////////////////////////////////////////

export const getUpdatedLocation =
  (router: Router) =>
  (args: GetUpdatedLocationArgs): RouterLocation => {
    const {
      pathname: currentPathname,
      search: currentSearch,
      hash: currentHash,
    } = router.getLocation()

    const currentParams = qs.parse(currentSearch, {ignoreQueryPrefix: true})

    // eslint-disable-next-line prefer-const
    let {pathname: newPathname, params: newParams, hash: newHash} = args

    if (newPathname === undefined) {
      newPathname = currentPathname
    }

    if (newHash === undefined) {
      newHash = currentHash
    }

    each(newParams, (v, k) => {
      currentParams[k] = v

      if (v === undefined) {
        delete currentParams[k]
      }
    })

    const newSearch = qs.stringify(currentParams, {addQueryPrefix: true})

    return {
      pathname: newPathname,
      search: newSearch,
      hash: newHash,
    }
  }

///////////////////////////////////////////////////////////////////////////////

export const updateUrlParams = (href: string, data: any): string => {
  const url = document.createElement('a')
  url.href = href

  const params = qs.parse(url.search, {ignoreQueryPrefix: true})

  each(data, (v, k) => {
    params[k] = v
  })

  url.search = qs.stringify(params, {
    addQueryPrefix: true,
    strictNullHandling: true,
  })

  return url.href
}

///////////////////////////////////////////////////////////////////////////////

export const getUpdatedUrl =
  (router: Router) =>
  (args: GetUpdatedLocationArgs): string => {
    const {pathname, search, hash} = getUpdatedLocation(router)(args)

    return pathname + search + hash
  }
