import axios, {AxiosPromise, AxiosRequestConfig} from 'axios'
import {Epik} from './Epik'
import {EpmsPaymentInfo} from './EpmsPaymentInfo'
import {EpmsSubscriptionInfo} from './EpmsSubscriptionInfo'
import {EpmsPaymentSourceInfo} from './EpmsPaymentSourceInfo'
import {PaymentMethod} from './PaymentMethod'
import {EpmsPaymentStatus, EpmsPaymentStatusReason} from './EpmsPaymentInfoBase'
import {EpmsSupporterInfo} from './EpmsSupporterInfo'

///////////////////////////////////////////////////////////////////////////////

export type CreateEpmsApiProxyArgs = {
  epik: Epik
}

export type FetchPaymentArgs = {
  epmsPaymentUuid: string
}

export type FetchPaymentStatusArgs = {
  epmsPaymentUuid: string
}

export type FetchSubscriptionArgs = {
  epmsSubscriptionUuid: string
}

export type FetchPaymentSourceArgs = {
  epmsPaymentSourceUuid: string
}

export type FetchSupporterArgs = {
  epmsSupporterUuid: string
}

export type CancelPaymentArgs = {
  epmsPaymentUuid: string
}

export type CancelSubscriptionArgs = {
  epmsSubscriptionUuid: string
}

export type UpdateSubscriptionArgs = {
  epmsSubscriptionUuid: string
  update: {
    payment_source_uuid: string
    currency_identifier: string
    amount: number
    timezone: string
    recurring_interval: string
  }
}

export type EpmsPaymentStatusInfo = {
  status: EpmsPaymentStatus
  reason: EpmsPaymentStatusReason
  timestamp: string
}

export interface IEpmsApiProxy {
  fetchPayment(args: FetchPaymentArgs): Promise<EpmsPaymentInfo>
  fetchPaymentStatus(args: FetchPaymentArgs): Promise<EpmsPaymentStatusInfo>
  fetchSubscription(args: FetchSubscriptionArgs): Promise<EpmsSubscriptionInfo>
  fetchPaymentSource(
    args: FetchPaymentSourceArgs,
  ): Promise<EpmsPaymentSourceInfo>
  fetchSupporter(args: FetchSupporterArgs): Promise<EpmsSupporterInfo>
  cancelPayment(args: CancelPaymentArgs): Promise<void>
  cancelSubscription(args: CancelSubscriptionArgs): Promise<void>
  updateSubscription(args: UpdateSubscriptionArgs): Promise<void>
}

///////////////////////////////////////////////////////////////////////////////

export class EpmsApiProxy implements IEpmsApiProxy {
  private epik: Epik

  constructor(args: CreateEpmsApiProxyArgs) {
    const {epik} = args
    this.epik = epik
  }

  async fetchPayment(args: FetchPaymentArgs): Promise<EpmsPaymentInfo> {
    const {epmsPaymentUuid} = args
    const {data} = await this.sendRequest<EpmsPaymentInfo>({
      method: 'get',
      url: `/epms/payments/${epmsPaymentUuid}`,
    })

    prepareEpmsPaymentInfo(data)

    return data
  }

  async fetchPaymentStatus(
    args: FetchPaymentStatusArgs,
  ): Promise<EpmsPaymentStatusInfo> {
    const {epmsPaymentUuid} = args
    const baseUrl = this.epik.config.epmsApiUrl

    const {data} = await axios.request({
      method: 'get',
      url: `${baseUrl}/statuses/payments/${epmsPaymentUuid}`,
    })

    return data
  }

  async fetchSubscription(
    args: FetchSubscriptionArgs,
  ): Promise<EpmsSubscriptionInfo> {
    const {epmsSubscriptionUuid} = args
    const {data} = await this.sendRequest<EpmsSubscriptionInfo>({
      method: 'get',
      url: `/epms/subscriptions/${epmsSubscriptionUuid}`,
    })

    return data
  }

  async fetchPaymentSource(
    args: FetchPaymentSourceArgs,
  ): Promise<EpmsPaymentSourceInfo> {
    const {epmsPaymentSourceUuid} = args
    const {data} = await this.sendRequest<EpmsPaymentSourceInfo>({
      method: 'get',
      url: `/epms/payment-sources/${epmsPaymentSourceUuid}`,
    })

    return data
  }

  async fetchSupporter(args: FetchSupporterArgs): Promise<EpmsSupporterInfo> {
    const {epmsSupporterUuid} = args
    const {data} = await this.sendRequest<EpmsSupporterInfo>({
      method: 'get',
      url: `/epms/supporters/${epmsSupporterUuid}`,
    })

    return data
  }

  async cancelPayment(args: CancelPaymentArgs): Promise<void> {
    const {epmsPaymentUuid} = args
    const baseUrl = this.epik.config.epmsApiUrl

    await axios.request({
      method: 'get',
      url: `${baseUrl}/return/${epmsPaymentUuid}`,
    })
  }

  async cancelSubscription(args: CancelSubscriptionArgs): Promise<void> {
    const {epmsSubscriptionUuid} = args
    await this.sendRequest({
      method: 'post',
      url: `/epms/subscriptions/${epmsSubscriptionUuid}/cancel`,
    })
  }

  async updateSubscription(args: UpdateSubscriptionArgs): Promise<void> {
    const {epmsSubscriptionUuid, update} = args
    await this.sendRequest({
      method: 'patch',
      url: `/epms/subscriptions/${epmsSubscriptionUuid}`,
      data: update,
    })
  }

  private sendRequest<T = any>(config: AxiosRequestConfig): AxiosPromise<T> {
    const baseUrl = this.epik.config.epmsProxyUrl
    const url = baseUrl + config.url

    return axios.request<T>({...config, url})
  }
}

///////////////////////////////////////////////////////////////////////////////

const prepareEpmsPaymentInfo = (data: EpmsPaymentInfo): void => {
  // File-based Sepa direct debit has a pending state of multiple days,
  // so we tell the supporter the payment was successful instead
  if (
    data.payment_method === PaymentMethod.SEPA_DD &&
    data.last_status === EpmsPaymentStatus.PENDING
  ) {
    data.last_status = EpmsPaymentStatus.SUCCEEDED
  }
}

///////////////////////////////////////////////////////////////////////////////

export const createEpmsApiProxy = (args: CreateEpmsApiProxyArgs) => {
  return new EpmsApiProxy(args)
}
