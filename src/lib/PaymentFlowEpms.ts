import axios from 'axios'
import {
  cloneDeep,
  each,
  get,
  omit,
  pickBy,
  set,
  startsWith,
  values,
} from 'lodash'
import {Epik} from './Epik'
import {EpmsPaymentConfig, PaymentConfig} from './PaymentConfig'
import {IPaymentFlow} from './PaymentFlow'
import {
  isCreditCardPaymentMethod,
  isSepaPaymentMethod,
  isTwintPaymentMethod,
  PaymentMethod,
} from './PaymentMethod'
import {EpikEventName} from './EpikEventer'
import {getUsersTimeZone} from './helpers/date'

///////////////////////////////////////////////////////////////////////////////

export type PaymentRequestData = EpmsPaymentConfig & {
  account_uuid: string
  test_mode: boolean
  locale: string
  payment_method?: string
  amount: {
    value: number
    currency: string
  }
  subscription?: {
    recurring_interval: string
    timezone: string
    custom_parameters?: {
      [key: string]: any
    }
    raisenow_parameters?: {
      [key: string]: any
    }
  }
}

export type PaymentResponseData = {
  action: {
    action_type: string
    url?: string
  }
  [key: string]: any
}

///////////////////////////////////////////////////////////////////////////////

export const SUPPORTER_FIELDS = {
  uuid: 'stored_customer_uuid',
  salutation: 'stored_customer_salutation',
  first_name: 'stored_customer_firstname',
  last_name: 'stored_customer_lastname',
  raw_name: 'stored_customer_raw_name',
  email: 'stored_customer_email',
  email_permission: 'stored_customer_email_permission',
  street: 'stored_customer_street',
  house_number: 'stored_customer_street_number',
  address_addendum: 'stored_customer_street2',
  postal_code: 'stored_customer_zip_code',
  city: 'stored_customer_city',
  country: 'stored_customer_country',
  region_level_1: 'stored_customer_state',
  raw_address: 'stored_customer_raw_address',
}

export const SUPPORTER_METADATA_FIELDS = {
  post_office_box: 'stored_customer_pobox',
}

export const RNW_INTEGRATION_FIELDS = {
  donation_receipt_requested: 'stored_customer_donation_receipt',
  message: 'stored_customer_message',
}

export const RNW_PRODUCT_FIELDS = {
  name: 'stored_rnw_product_name',
  version: 'stored_rnw_product_version',
  uuid: 'stored_rnw_widget_uuid',
  source_url: 'stored_rnw_source_url',
}

const IGNORED_FIELDS = ['stored_customer_birthdate']

///////////////////////////////////////////////////////////////////////////////

export class PaymentFlowEpms implements IPaymentFlow {
  async start(epik: Epik, payment: PaymentConfig) {
    const epmsPaymentRequestData = await preparePaymentData(payment, epik)

    if (payment.epms_subscription_uuid) {
      // update subscription
      epik.config.debug && console.debug('[epik] update subscription')

      try {
        await epik.events[
          EpikEventName.UPDATE_EPMS_SUBSCRIPTION_START
        ].publish()
        const epmsPaymentSourceRequestData = omit(
          epmsPaymentRequestData,
          'subscription',
        )

        // todo: skip it when it will be supported on backend side
        const epmsSubscriptionUpdateRequestData = {
          currency_identifier: epmsPaymentRequestData.amount.currency,
          amount: epmsPaymentRequestData.amount.value,
          timezone: epmsPaymentRequestData.subscription.timezone,
          recurring_interval:
            epmsPaymentRequestData.custom_parameters
              ?.rnw_recurring_interval_name,
        }

        const epmsPaymentSourceResponseData = await createPaymentSource(
          epmsPaymentSourceRequestData,
          epik,
        )
        const payment_source_uuid = get(
          epmsPaymentSourceResponseData,
          'objects.payment_source_uuid',
        )

        await epik.epmsProxy.updateSubscription({
          epmsSubscriptionUuid: payment.epms_subscription_uuid,
          update: {
            ...epmsSubscriptionUpdateRequestData,
            payment_source_uuid,
          },
        })

        await epik.events[EpikEventName.UPDATE_EPMS_SUBSCRIPTION_END].publish({
          epmsSubscriptionUuid: payment.epms_subscription_uuid,
        })
      } catch (error) {
        epik.config.debug && console.debug('[epik] update subscription error')

        if (error.response?.data) {
          await epik.events[
            EpikEventName.UPDATE_EPMS_SUBSCRIPTION_ERROR
          ].publish({
            epmsSubscriptionUuid: payment.epms_subscription_uuid,
            error: error.response.data,
          })
        } else {
          throw error
        }
      }
    } else {
      // send payment
      epik.config.debug && console.debug('[epik] send payment')

      try {
        await epik.events[EpikEventName.SEND_EPMS_PAYMENT_START].publish({
          epmsPaymentRequestData,
        })

        let epmsPaymentResponseData

        if (epmsPaymentRequestData.subscription) {
          epmsPaymentResponseData = await createPaymentSource(
            epmsPaymentRequestData,
            epik,
          )
        } else {
          epmsPaymentResponseData = await sendPayment(
            epmsPaymentRequestData,
            epik,
          )
        }

        await epik.events[EpikEventName.SEND_EPMS_PAYMENT_END].publish({
          epmsPaymentResponseData,
        })
      } catch (error) {
        epik.config.debug && console.debug('[epik] send payment error')

        if (error.response?.data) {
          const epmsPaymentResponseError = error.response?.data
          await epik.events[EpikEventName.SEND_EPMS_PAYMENT_ERROR].publish({
            epmsPaymentResponseError,
          })
        } else {
          throw error
        }
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////////

const sendPayment = async (
  requestData: Partial<PaymentRequestData>,
  epik: Epik,
): Promise<PaymentResponseData> => {
  const {data} = await axios.request<PaymentResponseData>({
    url: epik.config.epmsApiUrl + '/payments',
    method: 'post',
    data: requestData,
  })

  return data

  // // emulate request
  // return {
  //   "action": {
  //     "action_type": "redirect",
  //     "url": "twint-issuer-1:\/\/:\/\/applinks\/?al_applink_data={\u0022app_action_type\u0022:\u0022TWINT_PAYMENT\u0022,\u0022extras\u0022:{\u0022code\u0022:87778},\u0022referer_app_link\u0022:{\u0022app_name\u0022:\u0022RaiseNow\u0022,\u0022target_url\u0022:\u0022\u0022,\u0022url\u0022:\u0022\u0022},\u0022version\u0022:\u00226.0\u0022}"
  //   }
  // }
}

const createPaymentSource = async (
  requestData: Partial<PaymentRequestData>,
  epik: Epik,
): Promise<PaymentResponseData> => {
  const {data} = await axios.request<PaymentResponseData>({
    url: epik.config.epmsApiUrl + '/payment-sources',
    method: 'post',
    data: requestData,
  })

  return data
}

const preparePaymentData = async (
  payment: PaymentConfig,
  epik: Epik,
): Promise<Partial<PaymentRequestData>> => {
  const data: Partial<PaymentRequestData> = {}

  prepareMainInfo(data, payment, epik)
  prepareSupporter(data, payment)
  prepareRnwIntegration(data, payment)
  prepareRnwProduct(data, payment)
  prepareRnwCoverFee(data, payment)
  prepareRnwAnalytics(data, payment)
  prepareCustomParameters(data, payment)
  preparePaymentInfo(data, payment)
  prepareProfile(data, payment, epik)
  prepareRecurringInfo(data, payment)

  return data
}

///////////////////////////////////////////////////////////////////////////////

const prepareMainInfo = (
  data: Partial<PaymentRequestData>,
  payment: PaymentConfig,
  epik: Epik,
) => {
  data.account_uuid = epik.config.epmsAccountUuid
  data.test_mode = payment.test_mode
  data.return_url = payment.return_url
  data.locale = payment.language
  data.create_supporter = !!payment.create_supporter
  data.supporter = cloneDeep(payment.supporter || {})
  data.raisenow_parameters = cloneDeep(payment.raisenow_parameters || {})
  data.custom_parameters = cloneDeep(payment.custom_parameters || {})
  data.payment_information = cloneDeep(payment.payment_information || {})
  data.create_payment_source = !!payment.create_payment_source
  data.amount = {
    value: payment.amount,
    currency: payment.currency.toUpperCase(),
  }
}

///////////////////////////////////////////////////////////////////////////////

const prepareProfile = (
  data: Partial<PaymentRequestData>,
  payment: PaymentConfig,
  epik: Epik,
) => {
  if (payment.profile) {
    data.profile = payment.profile
  } else {
    const key = isCreditCardPaymentMethod(payment.payment_method)
      ? 'card'
      : payment.payment_method
    data.profile = epik.config.paymentMethodProfile[key]
  }
}

///////////////////////////////////////////////////////////////////////////////

const prepareSupporter = (
  data: Partial<PaymentRequestData>,
  payment: PaymentConfig,
) => {
  each(SUPPORTER_FIELDS, (v, k) => {
    const existingValue = get(payment, `supporter.${k}`)

    // don't override if field is already set
    if (payment[v] !== undefined && existingValue === undefined) {
      set(data, `supporter.${k}`, payment[v])
    }
  })

  each(SUPPORTER_METADATA_FIELDS, (v, k) => {
    const existingValue = get(payment, `supporter.metadata.${k}`)

    // don't override if field is already set
    if (payment[v] !== undefined && existingValue === undefined) {
      set(data, `supporter.metadata.${k}`, payment[v])
    }
  })

  // Add fields with "stored_customer_" prefix
  // (except specified ones)
  // to "customer.metadata"
  let info = pickBy(payment, (v, k) => {
    return startsWith(k, 'stored_customer_')
  })
  info = omit(info, [
    ...values(SUPPORTER_FIELDS),
    ...values(SUPPORTER_METADATA_FIELDS),
    ...values(RNW_INTEGRATION_FIELDS),
    ...values(RNW_PRODUCT_FIELDS),
    ...IGNORED_FIELDS,
  ])

  each(info, (v, k) => {
    const key = k.replace(/^stored_customer_/, '')
    const existingValue = get(payment, `supporter.metadata.${key}`)

    // don't override if field is already set
    if (v !== undefined && existingValue === undefined) {
      set(data, `supporter.metadata.${key}`, v)
    }
  })

  // Split stored_customer_birthdate
  if (payment.stored_customer_birthdate) {
    const [birth_year, birth_month, birth_day] =
      payment.stored_customer_birthdate.split('-')

    if (birth_year) {
      set(data, `supporter.metadata.birth_year`, birth_year)
    }

    if (birth_month) {
      set(data, `supporter.metadata.birth_month`, birth_month)
    }

    if (birth_day) {
      set(data, `supporter.metadata.birth_day`, birth_day)
    }
  }
}

///////////////////////////////////////////////////////////////////////////////

const prepareRnwIntegration = (
  data: Partial<PaymentRequestData>,
  payment: PaymentConfig,
) => {
  each(RNW_INTEGRATION_FIELDS, (v, k) => {
    const existingValue = get(payment, `raisenow_parameters.integration.${k}`)

    // don't override if field is already set
    if (payment[v] !== undefined && existingValue === undefined) {
      // value is casted to string
      // todo: refactor with payment team to not cast values
      set(
        data,
        `raisenow_parameters.integration.${k}`,
        `${payment[v] as string}`,
      )
    }
  })
}

///////////////////////////////////////////////////////////////////////////////

const prepareRnwProduct = (
  data: Partial<PaymentRequestData>,
  payment: PaymentConfig,
) => {
  each(RNW_PRODUCT_FIELDS, (v, k) => {
    const existingValue = get(payment, `raisenow_parameters.product.${k}`)

    // don't override if field is already set
    if (payment[v] !== undefined && existingValue === undefined) {
      // value is casted to string
      // todo: refactor with payment team to not cast values
      set(data, `raisenow_parameters.product.${k}`, `${payment[v] as string}`)
    }
  })
}

///////////////////////////////////////////////////////////////////////////////

const prepareRnwCoverFee = (
  data: Partial<PaymentRequestData>,
  payment: PaymentConfig,
) => {
  // Add fields with "stored_rnw_cover_fee_" prefix
  // to "raisenow_parameters.cover_fee"
  const info = pickBy(payment, (v, k) => {
    return startsWith(k, 'stored_rnw_cover_fee_')
  })

  each(info, (v, k) => {
    const key = k.replace(/^stored_rnw_cover_fee_/, '')
    const existingValue = get(payment, `raisenow_parameters.cover_fee.${key}`)

    // don't override if field is already set
    if (v !== undefined && existingValue === undefined) {
      // value is casted to string
      // todo: refactor with payment team to not cast values
      set(data, `raisenow_parameters.cover_fee.${key}`, `${v as string}`)
    }
  })
}

///////////////////////////////////////////////////////////////////////////////

const prepareRnwAnalytics = (
  data: Partial<PaymentRequestData>,
  payment: PaymentConfig,
) => {
  // Add fields with "stored_rnw_analytics_" prefix
  // to "raisenow_parameters.analytics"
  const info = pickBy(payment, (v, k) => {
    return startsWith(k, 'stored_rnw_analytics_')
  })

  each(info, (v, k) => {
    const key = k.replace(/^stored_rnw_analytics_/, '')
    const existingValue = get(payment, `raisenow_parameters.analytics.${key}`)

    // don't override if field is already set
    if (v !== undefined && existingValue === undefined) {
      // value is casted to string
      // todo: refactor with payment team to not cast values
      set(data, `raisenow_parameters.analytics.${key}`, `${v as string}`)
    }
  })
}

///////////////////////////////////////////////////////////////////////////////

const prepareCustomParameters = (
  data: Partial<PaymentRequestData>,
  payment: PaymentConfig,
) => {
  // Add fields with "stored_" prefix
  // (except ones with "stored_customer_" prefix and some others)
  // to "custom_parameters"
  let info = pickBy(payment, (v, k) => {
    return (
      startsWith(k, 'stored_') &&
      !startsWith(k, 'stored_customer_') &&
      !startsWith(k, 'stored_rnw_cover_fee_') &&
      !startsWith(k, 'stored_rnw_analytics_')
    )
  })
  info = omit(info, [
    ...values(SUPPORTER_FIELDS),
    ...values(SUPPORTER_METADATA_FIELDS),
    ...values(RNW_INTEGRATION_FIELDS),
    ...values(RNW_PRODUCT_FIELDS),
    ...IGNORED_FIELDS,
  ])

  each(info, (v, k) => {
    const key = k.replace(/^stored_/, '')
    const existingValue = get(payment, `custom_parameters.${key}`)

    // don't override if field is already set
    if (v !== undefined && existingValue === undefined) {
      // value is casted to string
      // todo: refactor with payment team to not cast values
      set(data, `custom_parameters.${key}`, `${v as string}`)
    }
  })
}

///////////////////////////////////////////////////////////////////////////////

const prepareRecurringInfo = (
  data: Partial<PaymentRequestData>,
  payment: PaymentConfig,
) => {
  if (payment.recurring || payment.stored_pseudo_recurring) {
    data.subscription = {
      recurring_interval: payment.recurring_interval,
      timezone: getUsersTimeZone(),
      custom_parameters: data.custom_parameters,
      raisenow_parameters: data.raisenow_parameters,
    }
  }
}

///////////////////////////////////////////////////////////////////////////////

const preparePaymentInfo = (
  data: Partial<PaymentRequestData>,
  payment: PaymentConfig,
) => {
  // Twint
  if (isTwintPaymentMethod(payment.payment_method)) {
    data.payment_method = PaymentMethod.TWINT
  }

  // Stripe
  else if (isCreditCardPaymentMethod(payment.payment_method)) {
    prepareCcPaymentInformation(data, payment)
  }

  // Sepa
  else if (isSepaPaymentMethod(payment.payment_method)) {
    data.payment_information = {
      iban: payment.iban,
      bic: payment.bic,
      timezone: getUsersTimeZone(),
    }
  }
}

///////////////////////////////////////////////////////////////////////////////

const prepareCcPaymentInformation = (
  data: Partial<PaymentRequestData>,
  payment: PaymentConfig,
) => {
  data.payment_method = PaymentMethod.CARD

  const PAYMENT_INFORMATION_FIELDS = {
    cardholder: 'card_holder_name',
    expiry_month: 'expm',
    expiry_year: 'expy',
    brand_code: 'payment_method',
    transaction_id: 'pci_proxy_transaction_id',
  }

  each(PAYMENT_INFORMATION_FIELDS, (v, k) => {
    const existingValue = get(payment, `payment_information.${k}`)

    // don't override if field is already set
    if (payment[v] !== undefined && existingValue === undefined) {
      set(data, `payment_information.${k}`, payment[v])
    }
  })
}
