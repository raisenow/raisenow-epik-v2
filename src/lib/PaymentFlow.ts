import {Epik} from './Epik'
import {PaymentConfig} from './PaymentConfig'
import {PaymentFlowEpms} from './PaymentFlowEpms'
import {PaymentFlowEpp} from './PaymentFlowEpp'
import {
  CreditCardPaymentMethods,
  isSupportedPaymentMethod,
  PaymentMethod,
} from './PaymentMethod'
import {findDefinedValue} from './helpers/values'

///////////////////////////////////////////////////////////////////////////////

export interface IPaymentFlow {
  start(epik: Epik, payment: PaymentConfig, window: Window): Promise<void>
}

export enum PaymentFlow {
  EPP = 'epp',
  EPMS = 'epms',
}

///////////////////////////////////////////////////////////////////////////////

const EppFlowPaymentMethods: PaymentMethod[] = [
  PaymentMethod.CREDIT_CARD,
  PaymentMethod.AMERICAN_EXPRESS,
  PaymentMethod.CYMBA,
  PaymentMethod.DINERS_CLUB,
  PaymentMethod.DIRECT_DEBIT,
  PaymentMethod.DISCOVERY,
  PaymentMethod.WEBBANKING_INDIA,
  PaymentMethod.ELEKTRONISCHES_LASTSCHRIFTVERFAHREN,
  PaymentMethod.ELECTRONIC_PAYMENT_SERVICE,
  PaymentMethod.JAPANESE_CREDIT_BUREAU,
  PaymentMethod.MAESTRO,
  PaymentMethod.MASTER_CARD,
  PaymentMethod.MASTER_PASS,
  PaymentMethod.PAYMENT_SLIP,
  PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER,
  PaymentMethod.PAY_PAL,
  PaymentMethod.POST_FINANCE,
  PaymentMethod.POST_FINANCE_CARD,
  PaymentMethod.SEPA_ONE,
  PaymentMethod.SMS,
  PaymentMethod.SOFORT,
  PaymentMethod.STRIPE,
  PaymentMethod.TWI,
  PaymentMethod.TWINT,
  PaymentMethod.VISA,
  PaymentMethod.MPOS,
  PaymentMethod.SWISS_QR_BILL,
]

const EpmsFlowPaymentMethods: PaymentMethod[] = [
  PaymentMethod.SEPA_DD,
  PaymentMethod.TWI,
  PaymentMethod.TWINT,
  ...CreditCardPaymentMethods,
]

export const getSupportedPaymentFlows = (
  paymentMethod: PaymentMethod,
): PaymentFlow[] => {
  if (!isSupportedPaymentMethod(paymentMethod)) {
    throw new Error(`Payment method "${paymentMethod}" is not supported`)
  }

  const flows = []

  if (EppFlowPaymentMethods.includes(paymentMethod)) {
    flows.push(PaymentFlow.EPP)
  }

  if (EpmsFlowPaymentMethods.includes(paymentMethod)) {
    flows.push(PaymentFlow.EPMS)
  }

  if (flows.length === 0) {
    throw new Error(
      `Payment method "${paymentMethod}" is not supported by any payment flow`,
    )
  }

  return flows
}

export const getPaymentFlowWithFallback = (
  payment: PaymentConfig,
): PaymentFlow => {
  if (!payment.payment_method) {
    return findDefinedValue(payment.flow, PaymentFlow.EPP)
  }

  const supportedPaymentFlows = getSupportedPaymentFlows(payment.payment_method)

  if (payment.flow && supportedPaymentFlows.includes(payment.flow)) {
    return payment.flow
  } else {
    return supportedPaymentFlows[0]
  }
}

export const createPaymentFlow = (paymentFlow: PaymentFlow): IPaymentFlow => {
  if (paymentFlow === PaymentFlow.EPP) {
    return new PaymentFlowEpp()
  } else if (paymentFlow === PaymentFlow.EPMS) {
    return new PaymentFlowEpms()
  }

  throw new Error(`Payment flow "${paymentFlow as string}" is not supported`)
}
