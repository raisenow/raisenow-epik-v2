import {PaymentCurrency} from './PaymentCurrency'
import {RecurringInterval} from './RecurringInterval'
import {PaymentMethod} from './PaymentMethod'
import {PaymentProvider} from './PaymentProvider'
import {PaymentFlow} from './PaymentFlow'

///////////////////////////////////////////////////////////////////////////////

export type PaymentConfig = {
  // epik
  return_parameters?: boolean

  // rnw custom fields
  stored_rnw_recurring_interval_name?: string
  stored_campaign_id?: string
  stored_campaign_subid?: string
  stored_es_ordered?: boolean
  stored_ezs_ordered?: boolean

  // general
  flow?: PaymentFlow
  amount?: number
  currency?: PaymentCurrency
  payment_method?: PaymentMethod
  payment_provider?: PaymentProvider

  // meta
  success_url?: string
  error_url?: string
  cancel_url?: string
  language?: string
  test_mode?: boolean
  mobile_mode?: boolean
  reqtype?: 'CAA' | any
  immediate_execution?: boolean
  further_rnw_interaction?: boolean | string

  // subscriptions
  recurring?: boolean
  recurring_interval?: RecurringInterval | string
  stored_pseudo_recurring?: boolean

  // customer profile
  stored_customer_title?: string
  stored_customer_salutation?: string
  stored_customer_firstname?: string
  stored_customer_lastname?: string
  stored_customer_raw_name?: string
  stored_customer_email?: string
  stored_customer_birthdate?: string
  stored_customer_email_permission?: boolean
  stored_customer_message?: string
  stored_customer_donation_receipt?: boolean

  // customer address
  stored_customer_street?: string
  stored_customer_street_number?: string
  stored_customer_street2?: string
  stored_customer_pobox?: string
  stored_customer_zip_code?: string
  stored_customer_city?: string
  stored_customer_country?: string
  stored_customer_state?: string
  stored_customer_raw_address?: string

  // credit cards
  card_holder_name?: string
  cardno?: string
  expm?: string
  expy?: string
  cvv?: string
  pci_proxy_transaction_id?: string

  // sms
  short_number?: number | string
  msisdn?: string

  // misc
  bank_name?: string
  bank_zip_code?: string
  bank_city?: string
  iban?: string
  bic?: string
  form_template?: string

  // payment slip
  payer_line1?: string
  payer_line2?: string
  payer_line3?: string
  payer_line4?: string
  refno?: string

  // hmac
  hmac?: string

  recaptcha?: string

  subscription_token?: string
  epms_subscription_uuid?: string

  [key: string]: any
} & EpmsPaymentConfig

export type EpmsPaymentConfig = {
  profile?: string
  return_url?: string
  create_supporter?: boolean
  supporter?: {
    uuid?: string
    salutation?: string
    first_name?: string
    last_name?: string
    raw_name?: string
    email?: string
    email_permission?: boolean
    street?: string
    house_number?: string
    address_addendum?: string
    postal_code?: string
    city?: string
    region_level_1?: string
    country?: string
    raw_address?: string
    metadata?: {
      birthdate?: string
      post_office_box?: string
      [key: string]: any
    }
  }
  raisenow_parameters?: {
    product?: {
      name?: string
      version?: string
      uuid?: string
      source_url?: string
      [key: string]: any
    }
    integration?: {
      donation_receipt_requested?: string
      message?: string
      [key: string]: any
    }
    cover_fee?: {
      [key: string]: any
    }
    analytics?: {
      [key: string]: any
    }
    solution?: {
      name?: string
      uuid?: string
      type?: string
      subtype?: string
      [key: string]: any
    }
  }
  custom_parameters?: {
    [key: string]: any
  }
  payment_information?: {
    [key: string]: any
  }

  create_payment_source?: boolean
  statement_descriptor?: string

  [key: string]: any
}
