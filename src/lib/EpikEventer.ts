import {values} from 'lodash'

///////////////////////////////////////////////////////////////////////////////

export enum EpikEventName {
  FETCH_PAYMENT_DATA_START = 'fetchPaymentDataStart',
  FETCH_PAYMENT_DATA_END = 'fetchPaymentDataEnd',
  EPP_PAYMENT_STATUS_POLL = 'eppPaymentStatusPoll',
  EPMS_PAYMENT_STATUS_POLL = 'epmsPaymentStatusPoll',
  SEND_EPP_PAYMENT_START = 'sendEppPaymentStart',
  SEND_EPMS_PAYMENT_START = 'sendEpmsPaymentStart',
  SEND_EPMS_PAYMENT_END = 'sendEpmsPaymentEnd',
  SEND_EPMS_PAYMENT_ERROR = 'sendEpmsPaymentError',
  UPDATE_EPMS_SUBSCRIPTION_START = 'updateEpmsSubscriptionStart',
  UPDATE_EPMS_SUBSCRIPTION_END = 'updateEpmsSubscriptionEnd',
  UPDATE_EPMS_SUBSCRIPTION_ERROR = 'updateEpmsSubscriptionError',
}

export type EpikEvent<T = any> = {
  eventName: EpikEventName
  data?: T
}

export type EpikEventHandler<T = any> = (
  event: EpikEvent<T>,
) => void | Promise<void>

export type EpikEvents = {
  [key in EpikEventName]?: EpikEventer
}

///////////////////////////////////////////////////////////////////////////////

export class EpikEventer {
  private eventName: EpikEventName
  private handlers: EpikEventHandler[] = []

  constructor(eventName: EpikEventName) {
    this.eventName = eventName
  }

  async publish(data?: any) {
    const event: EpikEvent = {eventName: this.eventName}

    if (data) {
      event.data = data
    }

    for (const handler of this.handlers) {
      await handler(event)
    }
  }

  subscribe(handler: EpikEventHandler) {
    this.handlers.push(handler)

    return () => {
      const idx = this.handlers.indexOf(handler)

      if (idx !== -1) {
        this.handlers.splice(idx, 1)
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////////

export const createEpikEvents = (): EpikEvents => {
  const result = {} as EpikEvents

  for (const eventName of values(EpikEventName)) {
    result[eventName] = new EpikEventer(eventName as EpikEventName)
  }

  return result
}
