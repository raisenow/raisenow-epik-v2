export enum PaymentCurrency {
  CHF = 'chf',
  EUR = 'eur',
  USD = 'usd',
}
