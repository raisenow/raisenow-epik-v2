import {PaymentCurrency} from './PaymentCurrency'
import {PaymentMethod} from './PaymentMethod'

///////////////////////////////////////////////////////////////////////////////

export type DetailedTransactionStatus = {
  created: string
  timestamp: string
  statusName: string
}

export enum TransactionStatus {
  SUCCESS = 'success',
  ERROR = 'error',
  CANCEL = 'cancel',
  PENDING = 'pending',
}

export type TransactionInfo = {
  epayment_status: TransactionStatus
  epp_transaction_id: string

  internal_return_executions: string
  transaction_id: string
  status: DetailedTransactionStatus[]
  created: string
  timestamp: string
  amount: number
  currency: PaymentCurrency
  payment_method: PaymentMethod
  payment_provider: string
  recurring_interval?: string
  further_rnw_interaction: boolean
  immediate_execution: boolean
  test_mode: boolean
  mobile_mode: boolean
  error_url: string
  success_url: string
  cancel_url: string

  language: string
  user_agent: string
  user_ip: string
  http_referer: string
  return_parameters: boolean
  subscription_id: string

  card_holder_name?: string
  expy?: string
  expm?: string

  stored_rnw_purpose_id?: string
  stored_rnw_recurring_interval_name?: string
  stored_es_ordered?: boolean
  stored_ezs_ordered?: boolean
  stored_chqr_ordered?: boolean

  error?: boolean
  success?: boolean
  cancel?: boolean

  iban: string
  bic: string

  stored_customer_salutation?: string
  stored_customer_firstname: string
  stored_customer_lastname: string
  stored_customer_email: string
  stored_customer_birthdate: string
  stored_customer_email_permission: boolean
  stored_customer_message: string
  stored_customer_donation_receipt: boolean
  stored_customer_street: string
  stored_customer_street_number: string
  stored_customer_street2: string
  stored_customer_pobox: string
  stored_customer_zip_code: string
  stored_customer_city: string
  stored_customer_country: string
  stored_customer_state: string

  subscription_token: string
  old_subscription_token?: string
  used_redirect_url: string

  espayment_pdflink?: string
  ezspayment_pdflink?: string
  chqr_pdflink?: string
  pdflink?: string

  [key: string]: any
}
