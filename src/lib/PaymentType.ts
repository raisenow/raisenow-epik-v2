import {uniq} from 'lodash'
import {isSupportedPaymentMethod, PaymentMethod} from './PaymentMethod'

///////////////////////////////////////////////////////////////////////////////

export enum PaymentType {
  ONE_TIME = 'onetime',
  RECURRING = 'recurring',
}

///////////////////////////////////////////////////////////////////////////////

export const OneTimePaymentMethods: PaymentMethod[] = [
  PaymentMethod.WEBBANKING_INDIA,
  PaymentMethod.ELEKTRONISCHES_LASTSCHRIFTVERFAHREN,
  PaymentMethod.ELECTRONIC_PAYMENT_SERVICE,
  PaymentMethod.JAPANESE_CREDIT_BUREAU, // cc
  PaymentMethod.MAESTRO, // cc
  PaymentMethod.MASTER_PASS,
  PaymentMethod.POST_FINANCE,
  PaymentMethod.SEPA_DD,
  PaymentMethod.SMS,
  PaymentMethod.SOFORT,
  PaymentMethod.TWINT,
  PaymentMethod.TWI,
  PaymentMethod.MPOS,
]

export const RecurringPaymentMethods: PaymentMethod[] = [
  PaymentMethod.CARD,
  PaymentMethod.CREDIT_CARD,
  PaymentMethod.AMERICAN_EXPRESS,
  PaymentMethod.CYMBA,
  PaymentMethod.DINERS_CLUB,
  PaymentMethod.DISCOVERY,
  PaymentMethod.MASTER_CARD,
  PaymentMethod.PAY_PAL,
  PaymentMethod.POST_FINANCE_CARD,
  PaymentMethod.SEPA_ONE,
  PaymentMethod.SEPA_DD,
  PaymentMethod.VISA,
  PaymentMethod.STRIPE,
  PaymentMethod.MPOS,
]

export const PseudoRecurringPaymentMethods: PaymentMethod[] = [
  PaymentMethod.DIRECT_DEBIT,
  PaymentMethod.PAYMENT_SLIP,
  PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER,
  PaymentMethod.SWISS_QR_BILL,
]

export const getSupportedPaymentTypes = (
  paymentMethod: PaymentMethod,
): PaymentType[] => {
  if (!isSupportedPaymentMethod(paymentMethod)) {
    throw new Error(`Payment method "${paymentMethod}" is not supported`)
  }

  const paymentTypes = []

  if (OneTimePaymentMethods.includes(paymentMethod)) {
    paymentTypes.push(PaymentType.ONE_TIME)
  }

  if (RecurringPaymentMethods.includes(paymentMethod)) {
    paymentTypes.push(PaymentType.RECURRING)
  }

  if (PseudoRecurringPaymentMethods.includes(paymentMethod)) {
    paymentTypes.push(PaymentType.RECURRING)
  }

  return uniq(paymentTypes)
}

export const isPaymentTypeSupported = (
  paymentMethod: PaymentMethod,
  paymentType: PaymentType,
): boolean => {
  return getSupportedPaymentTypes(paymentMethod).includes(paymentType)
}
