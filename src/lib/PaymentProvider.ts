import {values} from 'lodash'

///////////////////////////////////////////////////////////////////////////////

export enum PaymentProvider {
  ADYEN = 'adyen',
  DATATRANS = 'datatrans',
  INGCOLLECT = 'ingcollect',
  STRIPE = 'stripe',
}

///////////////////////////////////////////////////////////////////////////////

export const isSupportedPaymentProvider = (
  paymentProvider: PaymentProvider,
) => {
  return values(PaymentProvider).includes(paymentProvider)
}
