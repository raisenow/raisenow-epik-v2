import {PaymentCurrency} from './PaymentCurrency'

///////////////////////////////////////////////////////////////////////////////

export type EpmsSubscriptionInfo = {
  uuid: string
  account_uuid: string
  organisation_uuid: string
  customer_uuid: string
  charged_by: string
  payment_source_uuid: string
  created: number
  amount: number
  currency: PaymentCurrency
  recurring_interval: string
  timezone: string
  next_due_date: number
  auto_deactivation_threshold: number
  status: EpmsSubscriptionStatus
  history: EpmsSubscriptionHistoryEntry[]
  custom_parameters: any
  raisenow_parameters: any
  supporter_uuid: string
}

export type EpmsSubscriptionHistoryEntry = {
  type: string
  reason: string
  created: number
}

export enum EpmsSubscriptionStatus {
  ACTIVE = 'active',
  CANCELLED = 'cancelled',
}
