import {cloneDeep} from 'lodash'
import {findDefinedValue} from './helpers/values'
import {PaymentMethod} from './PaymentMethod'

///////////////////////////////////////////////////////////////////////////////

export type EpikConfig = {
  debug?: boolean
  testMode?: boolean
  language?: string
  reqtype?: 'CAA'
  furtherRnwInteraction?: boolean
  returnParameters?: boolean
  successUrl?: string
  errorUrl?: string
  cancelUrl?: string
  paymentStatusPollForceRequestsCount?: number
  paymentStatusPollInterval?: number
  skipCvv?: boolean
  requireSwissQrBillReferenceNumber?: boolean
  autoFetchReferenceNumber?: boolean

  eppEnv?: 'prod' | 'stage'
  eppApiKey?: string
  eppMerchantId?: string
  eppApiUrl?: string
  eppProxyUrl?: string

  epmsEnv?: 'prod' | 'stage'
  epmsApiUrl?: string
  epmsProxyUrl?: string
  epmsAccountUuid?: string

  paymentMethodProfile?: {
    [key in PaymentMethod]?: string
  }
}

///////////////////////////////////////////////////////////////////////////////

export const createEpikConfig = (config: EpikConfig = {}): EpikConfig => {
  config = cloneDeep(config) || {}

  const eppApiKeyDefault = process.env.EPP_API_KEY_DEFAULT

  config.debug = findDefinedValue(config.debug, false)
  config.eppApiKey = findDefinedValue(config.eppApiKey, eppApiKeyDefault)
  config.eppEnv = findDefinedValue(config.eppEnv, 'prod')
  config.epmsEnv = findDefinedValue(config.epmsEnv, 'prod')

  /////////////////////////////////////////////////////////////////////////////

  const eppApiUrlStage = process.env.EPP_API_URL_STAGE
  const eppApiUrlProd = process.env.EPP_API_URL_PROD
  const eppApiUrl = config.eppEnv === 'stage' ? eppApiUrlStage : eppApiUrlProd

  config.eppApiUrl = findDefinedValue(config.eppApiUrl, eppApiUrl)

  /////////////////////////////////////////////////////////////////////////////

  const epmsApiUrlStage = process.env.EPMS_API_URL_STAGE
  const epmsApiUrlProd = process.env.EPMS_API_URL_PROD
  const epmsApiUrl =
    config.epmsEnv === 'stage' ? epmsApiUrlStage : epmsApiUrlProd

  config.epmsApiUrl = findDefinedValue(config.epmsApiUrl, epmsApiUrl)

  /////////////////////////////////////////////////////////////////////////////

  const eppProxyUrlStage = process.env.EPP_PROXY_URL_STAGE
  const eppProxyUrlProd = process.env.EPP_PROXY_URL_PROD
  const eppProxyUrl =
    config.eppEnv === 'stage' ? eppProxyUrlStage : eppProxyUrlProd

  config.eppProxyUrl = findDefinedValue(config.eppProxyUrl, eppProxyUrl)

  /////////////////////////////////////////////////////////////////////////////

  const epmsProxyUrlStage = process.env.EPMS_PROXY_URL_STAGE
  const epmsProxyUrlProd = process.env.EPMS_PROXY_URL_PROD
  const epmsProxyUrl =
    config.epmsEnv === 'stage' ? epmsProxyUrlStage : epmsProxyUrlProd

  config.epmsProxyUrl = findDefinedValue(config.epmsProxyUrl, epmsProxyUrl)

  /////////////////////////////////////////////////////////////////////////////

  config.testMode = findDefinedValue(config.testMode, true)
  config.language = findDefinedValue(config.language, 'en')
  config.reqtype = findDefinedValue(config.reqtype, 'CAA')
  config.furtherRnwInteraction = findDefinedValue(
    config.furtherRnwInteraction,
    false,
  )
  config.returnParameters = findDefinedValue(config.returnParameters, false)
  config.paymentStatusPollForceRequestsCount = findDefinedValue(
    config.paymentStatusPollForceRequestsCount,
    5,
  )
  config.paymentStatusPollInterval = findDefinedValue(
    config.paymentStatusPollInterval,
    1000,
  )
  config.skipCvv = findDefinedValue(config.skipCvv, false)
  config.requireSwissQrBillReferenceNumber = findDefinedValue(
    config.requireSwissQrBillReferenceNumber,
    false,
  )
  config.autoFetchReferenceNumber = findDefinedValue(
    config.autoFetchReferenceNumber,
    false,
  )

  return config
}
