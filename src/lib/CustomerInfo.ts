export type CustomerInfo = {
  customer_id?: string
  email?: string
  merchant_id?: string

  stored_customer_language?: string
  stored_customer_salutation?: string
  stored_customer_firstname?: string
  stored_customer_lastname?: string
  stored_customer_raw_name?: string
  stored_customer_email?: string
  stored_customer_email_permission?: boolean
  stored_customer_donation_receipt?: boolean
  stored_customer_birthdate?: string
  stored_customer_street?: string
  stored_customer_street_number?: string
  stored_customer_street2?: string
  stored_customer_pobox?: string
  stored_customer_zip_code?: string
  stored_customer_city?: string
  stored_customer_country?: string
  stored_customer_state?: string
  stored_customer_raw_address?: string

  [key: string]: any
}
