import {isArray} from 'lodash'
import {PaymentConfig} from './PaymentConfig'

///////////////////////////////////////////////////////////////////////////////

export type PaymentErrors = {
  [key in keyof PaymentConfig]?: DetailedPaymentError[]
}

export type DetailedPaymentError = {
  code: string
  message: string
}

///////////////////////////////////////////////////////////////////////////////

export const addError = (
  errors: PaymentErrors,
  key: keyof PaymentErrors,
  error: DetailedPaymentError,
): PaymentErrors => {
  if (!isArray(errors[key])) {
    errors[key] = []
  }

  errors[key].push(error)

  return errors
}
