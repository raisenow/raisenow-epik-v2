export type CreateFormOptions = {
  target?: '_self' | string
  action?: string
  method?: 'get' | 'post'
  hidden?: boolean
}

///////////////////////////////////////////////////////////////////////////////

export const createForm = (
  document: Document,
  options: CreateFormOptions,
): HTMLFormElement => {
  const form = document.createElement('form')

  options = options || {}
  options.target = options.target || '_self'
  options.method = options.method || 'post'

  form.target = options.target
  form.method = options.method

  if (options.action) {
    form.action = options.action
  }

  if (options.hidden === true) {
    form.setAttribute('style', 'display: none;')
  }

  return form
}

export const createAndInjectForm = (
  document: Document,
  options: CreateFormOptions,
  targetSelector?: string,
): HTMLFormElement => {
  const form = createForm(document, options)
  injectHtmlElement(document, form, targetSelector)

  return form
}

export const injectHtmlElement = (
  document: Document,
  element: HTMLElement,
  targetSelector?: string,
) => {
  if (targetSelector) {
    const target = document.querySelector<HTMLElement>(targetSelector)

    if (!target) {
      throw new Error(
        `Could not inject element, target selector "${targetSelector}" is invalid`,
      )
    }

    // clear previous contents
    target.innerHTML = ''
    target.appendChild(element)
  } else {
    document.body.appendChild(element)
  }
}

export const fillForm = (
  document: Document,
  form: HTMLElement,
  data: {[key: string]: number | string | boolean},
): void => {
  for (const key in data) {
    const value = data[key]

    const input = document.createElement('input')
    input.setAttribute('type', 'text')
    input.setAttribute('name', key)
    input.setAttribute('placeholder', key)
    input.setAttribute('value', value as string)
    form.appendChild(input)
  }
}
