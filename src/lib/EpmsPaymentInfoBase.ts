import {PaymentMethod} from './PaymentMethod'
import {PaymentProvider} from './PaymentProvider'

///////////////////////////////////////////////////////////////////////////////

export type EpmsPaymentInfoBase = {
  uuid: string

  test_mode: boolean

  account_uuid: string
  organisation_uuid: string

  payment_method: PaymentMethod
  payment_provider: PaymentProvider

  metadata: EpmsPaymentMetadataEntry[]

  custom_parameters: {
    [key: string]: any
  }

  supporter_uuid: string | null

  created: number
  status_history: EpmsPaymentStatusHistoryEntry[]
  last_status: EpmsPaymentStatus
  last_status_reason: EpmsPaymentStatusReason
  last_status_timestamp: number

  method_profile_uuid: string
  provider_profile_uuid: string
  brand_code?: string
}

export type EpmsPaymentMetadataEntry = {
  type: string
  group: string
  name: string
  value_string: string
  value?: string // deprecated
  created: string
}

export enum EpmsPaymentStatus {
  PENDING = 'pending',
  SUCCEEDED = 'succeeded',
  FAILED = 'failed',
  REFUNDED = 'refunded',
  PARTIALLY_REFUNDED = 'partially_refunded',
}

export enum EpmsPaymentStatusReason {
  // PENDING
  NEW = 'new',
  PENDING = 'pending',
  UNKNOWN = 'unknown',

  // SUCCEEDED
  SUCCEEDED = 'succeeded',

  // FAILED
  ERROR = 'error',
  USER_TIMEOUT = 'user_timeout',
  USER_ABORTED = 'user_aborted',
  DECLINED = 'declined',

  // REFUNDED
  REFUNDED = 'refunded',

  // PARTIALLY_REFUNDED
  PARTIALLY_REFUNDED = 'partially_refunded',
}

export type EpmsPaymentStatusHistoryEntry = {
  status: EpmsPaymentStatus
  reason: EpmsPaymentStatusReason
  timestamp: string
}

///////////////////////////////////////////////////////////////////////////////

/*
Argument "path" is a string which must contain 3 segments joined by dots:
"<type>.<group>.<name>".

For example "raisenow.reference_codes.creditor_reference" is split as:
- type: "raisenow"
- group: "reference_codes"
- name: "creditor_reference"

More info about metadata field, how it's being set and stored is described in the wiki:
https://raisenow.atlassian.net/l/c/NZXLAriY

Info about payment entity:
https://raisenow.atlassian.net/l/c/KaEToxaY
 */

export const getEpmsPaymentMetadataEntryValue = (
  metadata: EpmsPaymentMetadataEntry[] | undefined,
  path: string,
): string | undefined => {
  const [type, group, name] = path.split('.')
  const entry = (metadata || []).find(
    (entry) =>
      entry['type'] === type &&
      entry['group'] === group &&
      entry['name'] === name,
  )

  // entry.value is for compatibility only (can be removed when backend is upgraded)

  return (entry as any)?.value || entry?.value_string
}
