import {values} from 'lodash'

///////////////////////////////////////////////////////////////////////////////

export enum PaymentMethod {
  // pseudo payment method, serves as
  // alias for all credit card types
  CREDIT_CARD = 'cc',
  CARD = 'card',

  AMERICAN_EXPRESS = 'amx',
  CYMBA = 'cym',
  DINERS_CLUB = 'din',
  DIRECT_DEBIT = 'dd',
  DISCOVERY = 'dis',
  WEBBANKING_INDIA = 'ebs',
  ELEKTRONISCHES_LASTSCHRIFTVERFAHREN = 'elv',
  ELECTRONIC_PAYMENT_SERVICE = 'eps',
  JAPANESE_CREDIT_BUREAU = 'jcb',
  MAESTRO = 'mae',
  MASTER_CARD = 'eca',
  MASTER_PASS = 'mpw',
  PAYMENT_SLIP = 'es',
  PAYMENT_SLIP_WITH_REFERENCE_NUMBER = 'ezs',
  SWISS_QR_BILL = 'chqr',
  PAY_PAL = 'pp',
  POST_FINANCE = 'pef',
  POST_FINANCE_CARD = 'pfc',
  SEPA_ONE = 'sod',
  SEPA_DD = 'sepa_dd',
  SMS = 'sms',
  SOFORT = 'dib',
  STRIPE = 'stp',
  TWI = 'twi',
  TWINT = 'twint',
  VISA = 'vis',
  MPOS = 'mpos',
}

///////////////////////////////////////////////////////////////////////////////

export const CreditCardPaymentMethods: PaymentMethod[] = [
  PaymentMethod.CARD,
  PaymentMethod.CREDIT_CARD,
  PaymentMethod.AMERICAN_EXPRESS,
  PaymentMethod.DINERS_CLUB,
  PaymentMethod.DISCOVERY,
  PaymentMethod.JAPANESE_CREDIT_BUREAU,
  PaymentMethod.MAESTRO,
  PaymentMethod.MASTER_CARD,
  PaymentMethod.VISA,
  PaymentMethod.STRIPE,
]

export const PostFinancePaymentMethods: PaymentMethod[] = [
  PaymentMethod.POST_FINANCE,
  PaymentMethod.POST_FINANCE_CARD,
]

export const PaymentMethodsWithDeferredStatus: PaymentMethod[] = [
  PaymentMethod.SMS,
  PaymentMethod.MPOS,
]

export const FallbackCreditCardPaymentMethod: PaymentMethod = PaymentMethod.VISA

export const isSupportedPaymentMethod = (paymentMethod: PaymentMethod) => {
  return values(PaymentMethod).includes(paymentMethod)
}

export const isCreditCardPaymentMethod = (
  paymentMethod: PaymentMethod | string,
) => {
  return CreditCardPaymentMethods.includes(paymentMethod as PaymentMethod)
}

export const isPostFinancePaymentMethod = (
  paymentMethod: PaymentMethod,
): boolean => {
  return PostFinancePaymentMethods.includes(paymentMethod)
}

export const isTwintPaymentMethod = (paymentMethod: PaymentMethod) =>
  paymentMethod === PaymentMethod.TWINT || paymentMethod === PaymentMethod.TWI

export const isSepaPaymentMethod = (paymentMethod: PaymentMethod) =>
  paymentMethod === PaymentMethod.SEPA_ONE ||
  paymentMethod === PaymentMethod.SEPA_DD

export const isPaymentMethodWithDeferredStatus = (
  paymentMethod: PaymentMethod,
): boolean => {
  return PaymentMethodsWithDeferredStatus.includes(paymentMethod)
}

export const getCreditCardType = (cardNumber: string): PaymentMethod => {
  if (!cardNumber) {
    return null
  }

  cardNumber = cardNumber.replace(/[A-z\s-]+/g, '')

  if (cardNumber.match(/^(4)/g)) {
    return PaymentMethod.VISA
  }
  if (cardNumber.match(/^(5[0-5])/g)) {
    return PaymentMethod.MASTER_CARD
  }
  if (cardNumber.match(/^3[4|7]/g)) {
    return PaymentMethod.AMERICAN_EXPRESS
  }
  if (cardNumber.match(/^36/g)) {
    return PaymentMethod.DINERS_CLUB
  }
  if (cardNumber.match(/^6((011)|5)/g)) {
    return PaymentMethod.DISCOVERY
  }
  if (cardNumber.match(/^(5[06789]|6)[0-9]{0,}$/g)) {
    return PaymentMethod.MAESTRO
  }
  if (cardNumber.match(/^35/g)) {
    return PaymentMethod.JAPANESE_CREDIT_BUREAU
  }

  return null
}
