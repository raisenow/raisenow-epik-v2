import {Epik} from './Epik'
import {PaymentConfig} from './PaymentConfig'
import {IPaymentFlow} from './PaymentFlow'
import {EpikEventName} from './EpikEventer'
import {createAndInjectForm, fillForm} from './Dom'
import {PaymentMethod} from './PaymentMethod'

///////////////////////////////////////////////////////////////////////////////

const preparePaymentData = (payment: PaymentConfig) => {
  const data = {...payment}

  if (payment.bic) {
    data.bankbic = payment.bic
    delete data.bic
  }

  if (
    [
      PaymentMethod.ELEKTRONISCHES_LASTSCHRIFTVERFAHREN,
      PaymentMethod.SEPA_ONE,
    ].includes(payment.payment_method)
  ) {
    data.bankiban = payment.iban
    delete data.iban
  }

  return data
}

////////////////////////////////////////////////////////////////////////////////

export class PaymentFlowEpp implements IPaymentFlow {
  start(epik: Epik, payment: PaymentConfig, window: Window): Promise<void> {
    return new Promise(() => {
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      setImmediate(async () => {
        const eppPaymentRequestData = preparePaymentData(payment)

        await epik.events[EpikEventName.SEND_EPP_PAYMENT_START].publish({
          eppPaymentRequestData,
        })

        const url = `${epik.config.eppApiUrl}/epayment/api/step/pay/merchant/${epik.config.eppApiKey}`
        const form = createAndInjectForm(window.document, {
          target: '_self',
          method: 'post',
          action: url,
          hidden: true,
        })

        fillForm(window.document, form, eppPaymentRequestData)

        form.submit()

        // don't resolve here so let form submit
      })
    })
  }
}
