export enum EpikQueryParam {
  EPP_TRANSACTION_ID = 'epp_transaction_id',
  SUBSCRIPTION_TOKEN = 'subscription_token',
  LEMA_SUBSCRIPTION_TOKEN = 'dds-subscription',
  CUSTOMER_TOKEN = 'customer_token',
  VIEW = 'rnw-view',
  EPMS_PAYMENT_UUID = 'epms_payment_uuid',
  EPMS_SUBSCRIPTION_UUID = 'epms_subscription_uuid',
}

///////////////////////////////////////////////////////////////////////////////

export const AllEpikQueryParams: EpikQueryParam[] = [
  EpikQueryParam.EPP_TRANSACTION_ID,
  EpikQueryParam.SUBSCRIPTION_TOKEN,
  EpikQueryParam.LEMA_SUBSCRIPTION_TOKEN,
  EpikQueryParam.CUSTOMER_TOKEN,
  EpikQueryParam.VIEW,
  EpikQueryParam.EPMS_PAYMENT_UUID,
  EpikQueryParam.EPMS_SUBSCRIPTION_UUID,
]

export const WatchedEpikQueryParams: EpikQueryParam[] = [
  EpikQueryParam.EPP_TRANSACTION_ID,
  EpikQueryParam.SUBSCRIPTION_TOKEN,
  EpikQueryParam.LEMA_SUBSCRIPTION_TOKEN,
  EpikQueryParam.CUSTOMER_TOKEN,
  EpikQueryParam.EPMS_PAYMENT_UUID,
  EpikQueryParam.EPMS_SUBSCRIPTION_UUID,
]
