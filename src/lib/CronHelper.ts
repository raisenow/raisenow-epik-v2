import {RecurringInterval} from './RecurringInterval'

///////////////////////////////////////////////////////////////////////////////

export class CronHelper {
  getRecurringIntervalExpression(
    recurringInterval: RecurringInterval | string,
  ): string {
    // Instead of parsing the date string as a regular expression, we
    // could use Intl.DateTimeFormat.prototype.formatToParts(), but
    // Internet Explorer does not support it.
    const dateInZurichParts = new RegExp(/^(\d+).(\d+).(\d+)/).exec(
      new Date()
        .toLocaleString('de-CH', {
          timeZone: 'Europe/Zurich',
        })
        .replace(/\u200e/g, ''), // IE puts LTR marks every here and there
    )
    const dateInZurich = new Date(
      parseInt(dateInZurichParts[3]),
      parseInt(dateInZurichParts[2]) - 1,
      parseInt(dateInZurichParts[1]),
    )
    const weekday = dateInZurich.getDay()
    const date = dateInZurich.getDate()
    const month = dateInZurich.getMonth() + 1

    // format is 'date of moth, month, weekday'

    if (recurringInterval === RecurringInterval.DAILY) {
      return ['*', '*', '*'].join(' ')
    } else if (recurringInterval === RecurringInterval.WEEKLY) {
      return ['*', '*', weekday].join(' ')
    } else if (recurringInterval === RecurringInterval.MONTHLY) {
      return [date, '*', '*'].join(' ')
    } else if (recurringInterval === RecurringInterval.QUARTERLY) {
      const months = this.getMonthsSequence(month, 4).join(',')
      return [date, months, '*'].join(' ')
    } else if (recurringInterval === RecurringInterval.SEMESTRAL) {
      const months = this.getMonthsSequence(month, 2).join(',')
      return [date, months, '*'].join(' ')
    } else if (recurringInterval === RecurringInterval.YEARLY) {
      return [date, month, '*'].join(' ')
    }

    // Return passed value if no match
    return recurringInterval
  }

  getRecurringIntervalName(expression: string): RecurringInterval {
    let matches = expression.match(
      /[^\s]+\s+[^\s]+\s+([^\s]+\s+[^\s]+\s+[^\s]+)/,
    )
    let monthMatches
    let len

    if (matches) {
      expression = matches[1]
    }

    if ((matches = expression.match(/([^\s]+)\s+([^\s]+)\s+([^\s]+)/))) {
      if (matches[1].match(/^\d+|L|\*$/)) {
        if (matches[3].match(/^\d+$/)) {
          return RecurringInterval.WEEKLY
        } else if (matches[2] === '*') {
          return RecurringInterval.MONTHLY
        } else {
          if (matches[2].match(/^\d+$/)) {
            return RecurringInterval.YEARLY
          } else if ((monthMatches = matches[2].match(/^(?:\*|\d+)\/(\d+)$/))) {
            if (monthMatches[1] == 3) {
              return RecurringInterval.QUARTERLY
            }

            if (monthMatches[1] == 6) {
              return RecurringInterval.SEMESTRAL
            }
          } else {
            len = matches[2].split(',').length

            if (len == 4) {
              return RecurringInterval.QUARTERLY
            }

            if (len == 2) {
              return RecurringInterval.SEMESTRAL
            }
          }
        }
      }
    }
  }

  getMonthsSequence(startMonth: number, repeatsInYear: number): number[] {
    if (startMonth < 1 || startMonth > 12) {
      throw new Error(
        'Invalid start month, must be one of: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12',
      )
    }

    if (12 % repeatsInYear !== 0) {
      throw new Error(
        'Invalid number of repeats, must be one of: 1, 2, 3, 4, 6',
      )
    }

    const interval = 12 / repeatsInYear
    const relativeOffset = startMonth - 1
    const months = []

    for (let i = 0; i < 12; i += interval) {
      const month = relativeOffset + i
      const monthBelow12 = month % 12
      const monthAbove0 = monthBelow12 + 1

      months.push(monthAbove0)
    }

    return months
  }
}

///////////////////////////////////////////////////////////////////////////////

export const createCronHelper = (): CronHelper => {
  return new CronHelper()
}
