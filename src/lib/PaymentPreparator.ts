import {findDefinedValue} from './helpers/values'
import {prepareExpirationYear} from './helpers/date'
import {prepareReturnUrl} from './helpers/returnUrl'
import {updateUrlParams} from './helpers/location'
import {Epik} from './Epik'
import {PaymentConfig} from './PaymentConfig'
import {
  FallbackCreditCardPaymentMethod,
  getCreditCardType,
  isCreditCardPaymentMethod,
  isSepaPaymentMethod,
  PaymentMethod,
} from './PaymentMethod'
import {filter, isBoolean, keys, startsWith} from 'lodash'
import {PaymentProvider} from './PaymentProvider'
import {
  PseudoRecurringPaymentMethods,
  RecurringPaymentMethods,
} from './PaymentType'
import {EpikQueryParam} from './EpikQueryParam'
import {getPaymentFlowWithFallback, PaymentFlow} from './PaymentFlow'

///////////////////////////////////////////////////////////////////////////////

export class PaymentPreparator {
  prepareFlow(payment: PaymentConfig): string[] {
    payment.flow = getPaymentFlowWithFallback(payment)

    return []
  }

  prepareMetadata(payment: PaymentConfig, epik: Epik): string[] {
    const isEpms = payment.flow === PaymentFlow.EPMS

    const relevantFields = [
      'flow',
      'payment_method',
      'currency',
      'amount',
      'test_mode',
      'mobile_mode',
      'language',
      'reqtype',
      'further_rnw_interaction',
      'return_parameters',
      'recaptcha',
      'subscription_token',
      'epms_subscription_uuid',
      'success_url',
      'error_url',
      'cancel_url',
      'country_code',
    ]

    if (isEpms) {
      relevantFields.push(
        'profile',
        'return_url',
        'create_supporter',
        'supporter',
        'raisenow_parameters',
        'custom_parameters',
        'payment_information',
      )
    }

    if (payment.payment_provider == PaymentProvider.INGCOLLECT) {
      payment.country_code = payment.stored_customer_country
    }

    payment.test_mode = findDefinedValue(
      payment.test_mode,
      epik.config.testMode,
      true,
    )
    payment.mobile_mode = false
    payment.language = findDefinedValue(payment.language, epik.config.language)
    payment.reqtype = findDefinedValue(payment.reqtype, epik.config.reqtype)
    payment.further_rnw_interaction = findDefinedValue(
      payment.further_rnw_interaction,
      epik.config.furtherRnwInteraction,
    )
      ? 'enabled'
      : 'disabled'
    payment.return_parameters = findDefinedValue(
      payment.return_parameters,
      epik.config.returnParameters,
      false,
    )

    // for epp
    payment.success_url = prepareReturnUrl(
      findDefinedValue(epik.config.successUrl, epik.window.location.href),
    )
    payment.error_url = prepareReturnUrl(
      findDefinedValue(epik.config.errorUrl, epik.window.location.href),
    )
    payment.cancel_url = prepareReturnUrl(
      findDefinedValue(epik.config.cancelUrl, epik.window.location.href),
    )

    // todo: remove it when backend make this parameter not mandatory
    // for epms
    payment.return_url = prepareReturnUrl(
      findDefinedValue(payment.return_url, epik.window.location.href),
    )

    const params = {}

    if (payment.subscription_token || payment.epms_subscription_uuid) {
      params[EpikQueryParam.VIEW] = 'subscription_payment_method_update_result'
    } else {
      params[EpikQueryParam.VIEW] = 'payment_result'
    }

    payment.success_url = updateUrlParams(payment.success_url, params)
    payment.error_url = updateUrlParams(payment.error_url, params)
    payment.cancel_url = updateUrlParams(payment.cancel_url, params)

    // todo: remove it when backend make this parameter not mandatory
    payment.return_url = updateUrlParams(payment.return_url, params)

    return relevantFields
  }

  prepareHmac(payment: PaymentConfig): string[] {
    const relevantFields = []

    if (payment.hmac) {
      relevantFields.push('hmac')
    }

    return relevantFields
  }

  prepareRecurring(payment: PaymentConfig, epik: Epik): string[] {
    const relevantFields = []

    let isRecurring = false
    let isPseudoRecurring = false

    if (payment.recurring || payment.stored_pseudo_recurring) {
      isRecurring = RecurringPaymentMethods.includes(payment.payment_method)
      isPseudoRecurring = PseudoRecurringPaymentMethods.includes(
        payment.payment_method,
      )
    }

    if (isRecurring) {
      relevantFields.push('recurring')

      payment.recurring = true
    }

    if (isPseudoRecurring) {
      relevantFields.push('stored_pseudo_recurring')

      payment.stored_pseudo_recurring = true
    }

    if (isRecurring || isPseudoRecurring) {
      relevantFields.push(
        'recurring_interval',
        'stored_rnw_recurring_interval_name',
        'stored_customer_email',
        'immediate_execution',
      )

      if (payment.recurring_interval) {
        payment.recurring_interval =
          epik.cronHelper.getRecurringIntervalExpression(
            payment.recurring_interval,
          )
      }

      if (!isBoolean(payment.immediate_execution)) {
        payment.immediate_execution = true
      }
    }

    return relevantFields
  }

  prepareCreditCard(payment: PaymentConfig, epik: Epik): string[] {
    const relevantFields = []

    if (isCreditCardPaymentMethod(payment.payment_method)) {
      relevantFields.push(
        'card_holder_name',
        'payment_provider',
        'create_payment_source',
        'expm',
        'expy',
      )

      const isEpp = payment.flow === PaymentFlow.EPP
      const isEpms = payment.flow === PaymentFlow.EPMS
      const skipCvv = !!epik.config.skipCvv
      const hasCardHolderName = !!payment.card_holder_name
      const hasFirstAndLastName =
        !!payment.stored_customer_firstname &&
        !!payment.stored_customer_lastname
      const hasRawName = !!payment.stored_customer_raw_name

      if (!hasCardHolderName) {
        if (hasFirstAndLastName) {
          payment.card_holder_name = `${payment.stored_customer_firstname} ${payment.stored_customer_lastname}`
        } else if (hasRawName) {
          payment.card_holder_name = payment.stored_customer_raw_name
        }
      }

      const hasExpirationYear = !!payment.expy

      if (hasExpirationYear) {
        payment.expy = prepareExpirationYear(payment.expy, payment.flow)
      }

      if (isEpp) {
        relevantFields.push('cardno')

        if (!skipCvv) {
          relevantFields.push('cvv')
        }

        const hasCardNumber = !!payment.cardno

        if (hasCardNumber) {
          payment.cardno = payment.cardno.replace(/[A-z\s-]+/g, '')
        }
      }

      if (isEpms) {
        relevantFields.push('pci_proxy_transaction_id')
      }

      this.autodetectAndSetCreditCardPaymentMethod(payment)
    }

    return relevantFields
  }

  private autodetectAndSetCreditCardPaymentMethod(payment: PaymentConfig) {
    const isStripe = payment.payment_provider === PaymentProvider.STRIPE
    const isEpp = payment.flow === PaymentFlow.EPP
    let paymentMethod

    if (isEpp) {
      if (isStripe) {
        payment.payment_method = PaymentMethod.STRIPE

        return
      }

      if (payment.cardno) {
        paymentMethod = getCreditCardType(payment.cardno)
      }

      if (!paymentMethod) {
        paymentMethod = FallbackCreditCardPaymentMethod
      }

      payment.payment_method = paymentMethod
    }
  }

  prepareDirectDebit(payment: PaymentConfig): string[] {
    const relevantFields = []
    const isDirectDebit = payment.payment_method === PaymentMethod.DIRECT_DEBIT

    if (isDirectDebit) {
      relevantFields.push(
        'iban',
        'bic',
        'bank_name',
        'bank_city',
        'bank_zip_code',
        'form_template',
      )
    }

    return relevantFields
  }

  prepareElv(payment: PaymentConfig): string[] {
    const relevantFields = []
    const isElv =
      payment.payment_method ===
      PaymentMethod.ELEKTRONISCHES_LASTSCHRIFTVERFAHREN

    if (isElv) {
      relevantFields.push('iban')
    }

    return relevantFields
  }

  prepareSepa(payment: PaymentConfig): string[] {
    const relevantFields = []
    const isSepa = isSepaPaymentMethod(payment.payment_method)

    if (isSepa) {
      relevantFields.push('iban', 'bic')
    }

    return relevantFields
  }

  async preparePaymentSlip(
    payment: PaymentConfig,
    epik: Epik,
  ): Promise<string[]> {
    const relevantFields = []
    const isEs = payment.payment_method === PaymentMethod.PAYMENT_SLIP
    const isEzs =
      payment.payment_method ===
      PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER
    const isChqr = payment.payment_method === PaymentMethod.SWISS_QR_BILL
    const requireChqrRefno = epik.config.requireSwissQrBillReferenceNumber
    const autoFetchRefno = epik.config.autoFetchReferenceNumber
    const hasPayerLine1 = !!payment.payer_line1
    const hasPayerLine2 = !!payment.payer_line2
    const hasPayerLine3 = !!payment.payer_line3
    const hasPayerLine4 = !!payment.payer_line4
    const hasAnyPayerLine =
      hasPayerLine1 || hasPayerLine2 || hasPayerLine3 || hasPayerLine4
    const hasRefno = !!payment.refno
    const hasCompany = !!payment.stored_customer_company
    const hasFirstAndLastName =
      !!payment.stored_customer_firstname && !!payment.stored_customer_lastname
    const hasRawName = !!payment.stored_customer_raw_name

    if (isEs) {
      relevantFields.push('stored_es_ordered', 'es_message')
    }

    if (isEzs) {
      relevantFields.push('stored_ezs_ordered', 'refno')
    }

    if (isChqr) {
      relevantFields.push('stored_chqr_ordered', 'refno', 'es_message')
    }

    if (isEs || isEzs || isChqr) {
      relevantFields.push(
        'payer_line1',
        'payer_line2',
        'payer_line3',
        'payer_line4',
      )
    }

    // If at least one of payer lines fields is provided in the payment object,
    // then we are skipping automatic population of these fields and use provided values instead.
    //
    // So if customer wants to have payer lines in some different format,
    // he must provide all these fields by himself.
    // Following logic should cover most of the customers.
    if ((isEs || isEzs || isChqr) && !hasAnyPayerLine) {
      const lines = []
      let name
      let companyName
      let address
      let location

      // Company (optional)
      if (hasCompany) {
        companyName = payment.stored_customer_company
      }

      // Name - "Firstname Lastname"
      if (hasFirstAndLastName) {
        name = [
          payment.stored_customer_firstname,
          payment.stored_customer_lastname,
        ].join(' ')
      } else if (hasRawName) {
        name = payment.stored_customer_raw_name
      }

      // "Raisenow - John Doe" || "Raisenow" || "John Doe"
      if (name || companyName) {
        lines.push([companyName, name].filter((v) => !!v).join(' - '))
      }

      // Street - "Dummystreet 42"
      address = [
        payment.stored_customer_street,
        payment.stored_customer_street_number,
      ]
        .filter((v) => !!v)
        .join(' ')

      // Street with optional P.O. box - "Dummystreet 42 / POBOX"
      address = [address, payment.stored_customer_pobox]
        .filter((v) => !!v)
        .join(' / ')

      if (address) {
        lines.push(address)
      }

      // Location - "CH-8005 Zürich"
      // eslint-disable-next-line prefer-const
      location = `${payment.stored_customer_country}-${payment.stored_customer_zip_code} ${payment.stored_customer_city}`
      lines.push(location)

      lines.forEach((line, idx) => {
        payment[`payer_line${idx + 1}`] = line
      })
    }

    if (isEzs || (isChqr && requireChqrRefno)) {
      if (!hasRefno && autoFetchRefno) {
        payment.refno = await epik.fetchReferenceNumber(
          payment.stored_campaign_id,
        )
      }
    }

    return relevantFields
  }

  prepareSms(payment: PaymentConfig): string[] {
    const relevantFields = []
    const isSms = payment.payment_method === PaymentMethod.SMS
    const hasMsisdn = !!payment.msisdn

    if (isSms) {
      relevantFields.push('msisdn')

      if (hasMsisdn) {
        payment.msisdn = ('' + payment.msisdn).replace(/\s/g, '')
      } else {
        payment.msisdn = ''
      }
    }

    return relevantFields
  }

  prepareCustomFields(payment: PaymentConfig): string[] {
    // following fields are handled separately so should be ignored here
    const ignore = [
      'stored_pseudo_recurring',
      'stored_payment_slip_delivery_type',
      'stored_es_ordered',
      'stored_ezs_ordered',
      'stored_chqr_ordered',
    ]

    return filter(keys(payment), (k) => {
      return startsWith(k, 'stored_') && !ignore.includes(k)
    })
  }
}

///////////////////////////////////////////////////////////////////////////////

export const createPaymentPreparator = (): PaymentPreparator => {
  return new PaymentPreparator()
}
