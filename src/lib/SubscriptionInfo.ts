import {PaymentCurrency} from './PaymentCurrency'
import {PaymentMethod} from './PaymentMethod'

///////////////////////////////////////////////////////////////////////////////

export enum SubscriptionStatus {
  ACTIVE = 'active',
  INACTIVE = 'inactive',
}

export type SubscriptionInfo = {
  status: SubscriptionStatus
  currency: PaymentCurrency
  amount: number
  payment_method: PaymentMethod
  payment_provider: string
  interval: string

  test_mode: boolean
  mobile_mode: boolean
  return_parameters: boolean
  immediate_execution: boolean
  language: string

  created_timestamp: string
  cancelled_timestamp: string
  next_charge_timestamp: string
  last_modification_timestamp: string

  success_url: string
  cancel_url: string
  error_url: string

  masked_cc?: string
  expm?: string
  expy?: string

  subscription_token?: string
  customer_token?: string

  customer?: {
    [key: string]: any
  }

  stored_rnw_recurring_interval_name?: string
  stored_rnw_purpose_id?: string
  stored_rnw_product_name?: string
  stored_rnw_product_version?: string
  stored_rnw_widget_uuid?: string
  stored_rnw_source_url?: string
  stored_es_ordered?: boolean
  stored_ezs_ordered?: boolean
  stored_chqr_ordered?: boolean

  stored_campaign_id?: string
  stored_campaign_subid?: string

  [key: string]: any
}
