export interface IPaymentStatusPoll {
  counter: number
  start(paymentId: string): void
  stop(): void
  isRunning(): boolean
}

export type CreatePaymentStatusPollArgs = {
  pollFn: (paymentId: string) => Promise<void>
  interval: number
}

///////////////////////////////////////////////////////////////////////////////

export class PaymentStatusPoll implements IPaymentStatusPoll {
  timerId: any
  counter: number
  paymentId: string
  pollFn: (paymentId: string) => Promise<void>
  interval: number

  constructor(args: CreatePaymentStatusPollArgs) {
    const {pollFn, interval} = args
    this.pollFn = pollFn
    this.interval = interval
  }

  start(paymentId: string) {
    if (!paymentId) {
      console.error('You must specify paymentId')
      return
    }

    if (this.paymentId) {
      console.error('Payment status polling already started')
      return
    }

    this.reset()
    this.paymentId = paymentId
    this.counter = 0

    void this.poll()
  }

  stop() {
    this.reset()
  }

  isRunning() {
    return !!this.paymentId && !!this.timerId
  }

  private reset() {
    clearTimeout(this.timerId)

    this.timerId = null
    this.paymentId = null
  }

  private async poll() {
    if (!this.paymentId) {
      this.reset()

      return
    }

    this.counter++

    await this.pollFn(this.paymentId)

    this.timerId = setTimeout(this.poll.bind(this), this.interval)
  }
}

///////////////////////////////////////////////////////////////////////////////

export const createPaymentStatusPoll = (
  args: CreatePaymentStatusPollArgs,
): PaymentStatusPoll => {
  return new PaymentStatusPoll(args)
}
