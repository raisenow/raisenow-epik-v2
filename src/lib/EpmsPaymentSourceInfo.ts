import {EpmsPaymentInfoBase} from './EpmsPaymentInfoBase'

///////////////////////////////////////////////////////////////////////////////

export type EpmsPaymentSourceInfo = EpmsPaymentInfoBase & {
  payment_source_parameters?: {
    iban?: string
    bic?: string
    mandate_id?: string
    mandate_signature_timestamp?: string
    name?: string
    timezone?: string

    [key: string]: any
  }
}
