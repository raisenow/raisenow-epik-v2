import {cloneDeep, merge} from 'lodash'
import {Epik} from './Epik'
import {PaymentConfig} from './PaymentConfig'
import {PaymentErrors} from './PaymentErrors'

///////////////////////////////////////////////////////////////////////////////

export class Payment {
  constructor(
    private epik: Epik,
    private config: PaymentConfig = {} as PaymentConfig,
  ) {}

  update(config: Partial<PaymentConfig>) {
    this.config = merge(this.config, config)
  }

  replace(config: PaymentConfig) {
    this.config = cloneDeep(config)
  }

  read(): PaymentConfig {
    return cloneDeep(this.config)
  }

  async send(): Promise<void> {
    return this.epik.sendPayment(this.config)
  }

  async validate(): Promise<PaymentErrors> {
    return this.epik.validatePayment(this.config)
  }
}

///////////////////////////////////////////////////////////////////////////////

export const createPayment = (epik: Epik, payment?: PaymentConfig): Payment => {
  return new Payment(epik, payment)
}
