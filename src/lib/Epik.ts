import {
  cloneDeep,
  each,
  isArray,
  keys,
  pick,
  isEqual,
  get,
  isEmpty,
} from 'lodash'
import pRetry from 'p-retry'
import {createCronHelper, CronHelper} from './CronHelper'
import {
  getLocationParams,
  GetUpdatedLocationArgs,
  getUpdatedUrl,
  updateUrlParams,
} from './helpers/location'
import {cleanBlankFields} from './helpers/values'
import {prepareReturnUrl} from './helpers/returnUrl'
import {CustomerInfo} from './CustomerInfo'
import {createEppApiProxy, IEppApiProxy} from './EppProxy'
import {
  createEpmsApiProxy,
  EpmsPaymentStatusInfo,
  IEpmsApiProxy,
} from './EpmsProxy'
import {createEpikConfig, EpikConfig} from './EpikConfig'
import {createPayment, Payment} from './Payment'
import {PaymentConfig} from './PaymentConfig'
import {PaymentErrors} from './PaymentErrors'
import {createPaymentFlow, IPaymentFlow} from './PaymentFlow'
import {createPaymentPreparator, PaymentPreparator} from './PaymentPreparator'
import {createPaymentValidator, PaymentValidator} from './PaymentValidator'
import {
  DetailedTransactionStatus,
  TransactionInfo,
  TransactionStatus,
} from './TransactionInfo'
import {SubscriptionInfo} from './SubscriptionInfo'
import {EpmsPaymentInfo} from './EpmsPaymentInfo'
import {EpmsSubscriptionInfo} from './EpmsSubscriptionInfo'
import {EpmsPaymentSourceInfo} from './EpmsPaymentSourceInfo'
import {createPaymentStatusPoll, PaymentStatusPoll} from './PaymentStatusPoll'
import {
  createEpikEvents,
  EpikEvent,
  EpikEventName,
  EpikEvents,
} from './EpikEventer'
import {isPaymentMethodWithDeferredStatus, PaymentMethod} from './PaymentMethod'
import {PaymentCurrency} from './PaymentCurrency'
import {EpikQueryParam, WatchedEpikQueryParams} from './EpikQueryParam'
import {createRouter, HistoryMode, Router} from '../router'
import {EpmsPaymentStatus} from './EpmsPaymentInfoBase'
import {EpmsSupporterInfo} from './EpmsSupporterInfo'

///////////////////////////////////////////////////////////////////////////////

export type EpikArgs = {
  config: EpikConfig
  win?: Window
  validator?: PaymentValidator
  preparator?: PaymentPreparator
  cronHelper?: CronHelper
  epikEvents?: EpikEvents
}

///////////////////////////////////////////////////////////////////////////////

export class Epik {
  window: Window
  config: EpikConfig
  eppProxy: IEppApiProxy
  epmsProxy: IEpmsApiProxy
  validator: PaymentValidator
  preparator: PaymentPreparator
  cronHelper: CronHelper
  events: EpikEvents
  eppPaymentStatusPoll: PaymentStatusPoll
  epmsPaymentStatusPoll: PaymentStatusPoll
  router: Router
  watchedQueryParams: {[key: string]: string}

  constructor(args: EpikArgs) {
    const {config, win, validator, preparator, cronHelper, epikEvents} = args

    this.config = config
    this.window = win || window
    this.eppProxy = createEppApiProxy({epik: this})
    this.epmsProxy = createEpmsApiProxy({epik: this})
    this.validator = validator || createPaymentValidator()
    this.preparator = preparator || createPaymentPreparator()
    this.cronHelper = cronHelper || createCronHelper()
    this.events = epikEvents || createEpikEvents()
    this.router = createRouter(HistoryMode.BROWSER)
    this.watchedQueryParams = {}

    this.initPaymentPolls()
    this.initLocationChangeHandling()
    this.initEventsHandling()
  }

  init() {
    this.handleLocationChange()
  }

  initPaymentPolls() {
    this.eppPaymentStatusPoll = createPaymentStatusPoll({
      pollFn: async (paymentId) => {
        try {
          const {epayment_status: paymentStatus} = await this.fetchTransaction(
            paymentId,
          )

          await this.events[EpikEventName.EPP_PAYMENT_STATUS_POLL].publish({
            paymentId,
            paymentStatus,
          })
        } catch (error) {
          if (error.response?.status === 404) {
            this.eppPaymentStatusPoll.stop()
          }
        }
      },
      interval: this.config.paymentStatusPollInterval,
    })

    this.epmsPaymentStatusPoll = createPaymentStatusPoll({
      pollFn: async (paymentId) => {
        try {
          const {status: paymentStatus} = await this.fetchEpmsPaymentStatus(
            paymentId,
          )
          await this.events[EpikEventName.EPMS_PAYMENT_STATUS_POLL].publish({
            paymentId,
            paymentStatus,
          })
        } catch (error) {
          if (error.response?.status === 404) {
            this.epmsPaymentStatusPoll.stop()
          }
        }
      },
      interval: this.config.paymentStatusPollInterval,
    })
  }

  private initLocationChangeHandling() {
    this.router.listen(this.handleLocationChange.bind(this))
  }

  handleLocationChange() {
    const params = getLocationParams(this.router)
    const watchedQueryParams = pick(params, WatchedEpikQueryParams)

    if (!isEqual(watchedQueryParams, this.watchedQueryParams)) {
      this.watchedQueryParams = watchedQueryParams
      void this.fetchAndPublishPaymentData()
      return
    }

    // We need to refetch subscription after it's updated
    if (
      params[EpikQueryParam.VIEW] ===
      'subscription_payment_method_update_result'
    ) {
      void this.fetchAndPublishPaymentData()
    }
  }

  private initEventsHandling() {
    const eventNames = [
      EpikEventName.FETCH_PAYMENT_DATA_START,
      EpikEventName.FETCH_PAYMENT_DATA_END,
      EpikEventName.SEND_EPP_PAYMENT_START,
      EpikEventName.SEND_EPMS_PAYMENT_START,
      EpikEventName.SEND_EPMS_PAYMENT_END,
      EpikEventName.SEND_EPMS_PAYMENT_ERROR,
      EpikEventName.UPDATE_EPMS_SUBSCRIPTION_START,
      EpikEventName.UPDATE_EPMS_SUBSCRIPTION_END,
      EpikEventName.UPDATE_EPMS_SUBSCRIPTION_ERROR,
      EpikEventName.EPP_PAYMENT_STATUS_POLL,
      EpikEventName.EPMS_PAYMENT_STATUS_POLL,
    ]

    each(eventNames, (eventName) => {
      this.events[eventName].subscribe((event: EpikEvent) => {
        this.config.debug &&
          console.debug('[epik]', event.eventName, event.data)
      })
    })

    this.events[EpikEventName.FETCH_PAYMENT_DATA_END].subscribe(
      async (event) => {
        if (event.data.transactionInfo) {
          this.tryStartEppPaymentStatusPoll({
            paymentId: event.data.transactionInfo?.epp_transaction_id,
            paymentStatus: event.data.transactionInfo?.epayment_status,
          })
        }

        if (event.data.epmsPaymentInfo) {
          this.tryStartEpmsPaymentStatusPoll({
            paymentId: event.data.epmsPaymentInfo?.uuid,
            paymentStatus: event.data.epmsPaymentInfo?.last_status,
          })
        }
      },
    )

    this.events[EpikEventName.EPP_PAYMENT_STATUS_POLL].subscribe((event) => {
      this.tryStopEppPaymentStatusPoll(event.data.paymentStatus)
    })

    this.events[EpikEventName.EPMS_PAYMENT_STATUS_POLL].subscribe((event) => {
      this.tryStopEpmsPaymentStatusPoll(event.data.paymentStatus)
    })

    this.events[EpikEventName.SEND_EPMS_PAYMENT_END].subscribe((event) => {
      const {epmsPaymentResponseData} = event.data

      if (epmsPaymentResponseData) {
        const action_type = get(epmsPaymentResponseData, 'action.action_type')
        let action_url = get(epmsPaymentResponseData, 'action.url')
        const payment_uuid = get(
          epmsPaymentResponseData,
          'objects.payment_uuid',
        )
        const subscription_uuid = get(
          epmsPaymentResponseData,
          'objects.subscription_uuid',
        )

        if (action_url) {
          // Analyse action URL to decide whether to directly open Twint app
          // (will be made easier with PAY-1376)
          const actionUrl = new URL(action_url)
          const {searchParams} = actionUrl
          const pairingToken = searchParams.get('token')

          if (pairingToken) {
            const returnAppScheme = searchParams.get('returnAppScheme')
            const returnAppPackage = searchParams.get('returnAppPackage')

            if (returnAppScheme) {
              action_url = `${returnAppScheme}://applinks/?al_applink_data=${JSON.stringify(
                {
                  app_action_type: 'TWINT_PAYMENT',
                  extras: {code: pairingToken},
                  referer_app_link: {
                    app_name: 'SMALL_BUSINESS_SOLUTION',
                  },
                  version: '6.0',
                },
              )}`
            }

            if (returnAppPackage) {
              action_url = `intent://payment#Intent;package=${returnAppPackage};action=ch.twint.action.TWINT_PAYMENT;scheme=twint;S.code=${pairingToken};S.startingOrigin=SMALL_BUSINESS_SOLUTION;end`
            }
          }
        }

        if (
          action_type === 'show_results' ||
          // If we’re asked to redirect to a non-HTTP URL, Epik is
          // probably opening a payment app. When returning to the
          // browser, the user should see the payment’s status.
          (action_type === 'redirect' &&
            action_url?.substring(0, 4) !== 'http') ||
          // Also the case that we’re not required to take action by
          // the payment backend (usually because there was an error)
          // must be handled
          action_type === 'none'
        ) {
          let returnUrl = prepareReturnUrl(this.window.location.href)
          const params = {}

          if (payment_uuid || subscription_uuid) {
            params[EpikQueryParam.VIEW] = 'payment_result'
          }

          if (payment_uuid) {
            params[EpikQueryParam.EPMS_PAYMENT_UUID] = payment_uuid
          }

          if (subscription_uuid) {
            params[EpikQueryParam.EPMS_SUBSCRIPTION_UUID] = subscription_uuid
          }

          // Update query parameters without browser redirect (state is preserved).
          if (!isEmpty(params)) {
            returnUrl = updateUrlParams(returnUrl, params)
            this.router.push(returnUrl)
          }
        }

        // Redirect to the checkout page
        if (action_type === 'redirect' && action_url) {
          window.location.href = action_url
        }
      }
    })

    this.events[EpikEventName.UPDATE_EPMS_SUBSCRIPTION_END].subscribe(
      (event) => {
        const {epmsSubscriptionUuid} = event.data || {}

        let returnUrl = prepareReturnUrl(this.window.location.href)
        const params = {}

        if (epmsSubscriptionUuid) {
          params[EpikQueryParam.VIEW] =
            'subscription_payment_method_update_result'
          params[EpikQueryParam.EPMS_SUBSCRIPTION_UUID] = epmsSubscriptionUuid
        }

        if (!isEmpty(params)) {
          returnUrl = updateUrlParams(returnUrl, params)
          this.router.push(returnUrl)
        }
      },
    )
  }

  /////////////////////////////////////////////////////////////////////////////

  updateCurrentUrl(args: GetUpdatedLocationArgs) {
    const updatedUrl = getUpdatedUrl(this.router)(args)

    this.router.push(updatedUrl)
  }

  /////////////////////////////////////////////////////////////////////////////

  async fetchAndPublishPaymentData() {
    await this.events[EpikEventName.FETCH_PAYMENT_DATA_START].publish()

    const params = getLocationParams(this.router)

    const eppTransactionId = params[EpikQueryParam.EPP_TRANSACTION_ID]
    let subscriptionToken = params[EpikQueryParam.SUBSCRIPTION_TOKEN]
    const lemaSubscriptionToken = params[EpikQueryParam.LEMA_SUBSCRIPTION_TOKEN]
    let customerToken = params[EpikQueryParam.CUSTOMER_TOKEN]
    const epmsPaymentUuid = params[EpikQueryParam.EPMS_PAYMENT_UUID]
    let epmsSubscriptionUuid = params[EpikQueryParam.EPMS_SUBSCRIPTION_UUID]
    let epmsPaymentSourceUuid = null
    let epmsSupporterUuid = null

    let transactionInfo: TransactionInfo = null
    let subscriptionInfo: SubscriptionInfo = null
    let customerInfo: CustomerInfo = null
    let epmsPaymentInfo: EpmsPaymentInfo = null
    let epmsSubscriptionInfo: EpmsSubscriptionInfo = null
    let epmsPaymentSourceInfo: EpmsPaymentSourceInfo = null
    let epmsSupporterInfo: EpmsSupporterInfo = null

    let transactionError = null
    let subscriptionError = null
    let customerError = null
    let epmsPaymentError = null
    let epmsSubscriptionError = null
    let epmsPaymentSourceError = null
    let epmsSupporterError = null

    // epp

    if (eppTransactionId) {
      try {
        transactionInfo = await this.fetchTransaction(eppTransactionId)

        if (transactionInfo?.subscription_token && !subscriptionToken) {
          subscriptionToken = transactionInfo.subscription_token
        }
      } catch (error) {
        console.log('fetch transaction error', error)
        transactionError = {code: 'NOT_FOUND'}
      }
    }

    if (subscriptionToken) {
      try {
        subscriptionInfo = await this.fetchSubscription(subscriptionToken)

        if (subscriptionInfo?.customer_token && !customerToken) {
          customerToken = subscriptionInfo.customer_token
        }
      } catch (error) {
        console.log('fetch subscription error', error)
        subscriptionError = {code: 'NOT_FOUND'}
      }
    }

    if (lemaSubscriptionToken) {
      try {
        subscriptionInfo = await this.fetchSubscription(lemaSubscriptionToken)
      } catch (error) {
        console.log('fetch subscription error', error)
        subscriptionError = {code: 'NOT_FOUND'}
      }
    }

    if (customerToken) {
      try {
        customerInfo = await this.fetchCustomer(customerToken)
      } catch (error) {
        console.log('fetch customer error', error)
        customerError = {code: 'NOT_FOUND'}
      }
    }

    // epms

    if (epmsPaymentUuid) {
      try {
        epmsPaymentInfo = await this.fetchEpmsPayment(epmsPaymentUuid)

        if (
          epmsPaymentInfo?.created_subscription_uuid &&
          !epmsSubscriptionUuid
        ) {
          epmsSubscriptionUuid = epmsPaymentInfo.created_subscription_uuid
        }
      } catch (error) {
        console.log('fetch epms payment error', error)
        epmsPaymentError = {code: 'NOT_FOUND'}
      }
    }

    if (epmsSubscriptionUuid) {
      try {
        epmsSubscriptionInfo = await this.fetchEpmsSubscription(
          epmsSubscriptionUuid,
        )

        if (epmsSubscriptionInfo) {
          epmsPaymentSourceUuid ||= epmsSubscriptionInfo.payment_source_uuid
          epmsSupporterUuid ||= epmsSubscriptionInfo.supporter_uuid
        }
      } catch (error) {
        console.log('fetch epms subscription error', error)
        epmsSubscriptionError = {code: 'NOT_FOUND'}
      }
    }

    if (epmsPaymentSourceUuid) {
      try {
        epmsPaymentSourceInfo = await this.fetchEpmsPaymentSource(
          epmsPaymentSourceUuid,
        )

        if (epmsPaymentSourceInfo) {
          epmsSupporterUuid ||= epmsPaymentSourceInfo.supporter_uuid
        }
      } catch (error) {
        console.log('fetch epms payment source error', error)
        epmsPaymentSourceError = {code: 'NOT_FOUND'}
      }
    }

    if (epmsSupporterUuid) {
      try {
        epmsSupporterInfo = await this.fetchEpmsSupporter(epmsSupporterUuid)
      } catch (error) {
        console.log('fetch epms supporter error', error)
        epmsSupporterError = {code: 'NOT_FOUND'}
      }
    }

    await this.events[EpikEventName.FETCH_PAYMENT_DATA_END].publish({
      transactionInfo,
      subscriptionInfo,
      customerInfo,
      epmsPaymentInfo,
      epmsSubscriptionInfo,
      epmsPaymentSourceInfo,
      epmsSupporterInfo,

      transactionError,
      subscriptionError,
      customerError,
      epmsPaymentError,
      epmsSubscriptionError,
      epmsPaymentSourceError,
      epmsSupporterError,
    })
  }

  /////////////////////////////////////////////////////////////////////////////

  createPayment(payment?: PaymentConfig): Payment {
    return createPayment(this, payment)
  }

  /////////////////////////////////////////////////////////////////////////////

  async sendPayment(payment: PaymentConfig): Promise<void> {
    payment = await this.preparePayment(payment)

    cleanBlankFields(payment)

    const paymentFlow = this.createPaymentFlow(payment)
    await paymentFlow.start(this, payment, this.window)
  }

  /////////////////////////////////////////////////////////////////////////////

  async validatePayment(payment: PaymentConfig): Promise<PaymentErrors | null> {
    payment = await this.preparePayment(payment)

    const errors = {
      ...this.validator.validateMetadata(payment),
      ...this.validator.validatePaymentMethod(payment),
      ...this.validator.validatePaymentProvider(payment),
      ...this.validator.validateRecurring(payment),
      ...this.validator.validateAmount(payment),
      ...this.validator.validateCurrency(payment),
      ...this.validator.validateCreditCard(payment, this),
      ...this.validator.validateDirectDebit(payment),
      ...this.validator.validateElv(payment),
      ...this.validator.validateSepa(payment),
      ...this.validator.validatePaymentSlip(payment, this),
      ...this.validator.validateSms(payment),
      ...this.validator.validateCustomer(payment),
      ...this.validator.validateCustomerAddress(payment),
    } as PaymentErrors

    return keys(errors).length > 0 ? errors : null
  }

  async preparePayment(payment: PaymentConfig): Promise<PaymentConfig> {
    payment = cloneDeep(payment)

    const relevantFields = [
      ...this.preparator.prepareFlow(payment),
      ...this.preparator.prepareMetadata(payment, this),
      ...this.preparator.prepareHmac(payment),
      ...this.preparator.prepareRecurring(payment, this),
      ...this.preparator.prepareCreditCard(payment, this),
      ...this.preparator.prepareDirectDebit(payment),
      ...this.preparator.prepareElv(payment),
      ...this.preparator.prepareSepa(payment),
      ...(await this.preparator.preparePaymentSlip(payment, this)),
      ...this.preparator.prepareSms(payment),
      ...this.preparator.prepareCustomFields(payment),
    ]

    return pick(payment, relevantFields) as PaymentConfig
  }

  /////////////////////////////////////////////////////////////////////////////
  // epp
  /////////////////////////////////////////////////////////////////////////////

  async fetchTransaction(eppTransactionId: string): Promise<TransactionInfo> {
    const {eppApiKey} = this.config
    let data = await this.eppProxy.fetchTransaction({
      eppApiKey,
      eppTransactionId,
    })

    data = this.prepareTransactionInfo(data)

    return data
  }

  async fetchSubscription(
    subscriptionToken: string,
  ): Promise<SubscriptionInfo> {
    const {eppApiKey} = this.config
    let data = await this.eppProxy.fetchSubscription({
      eppApiKey,
      subscriptionToken,
    })

    data.subscription_token = subscriptionToken
    data = this.prepareSubscriptionInfo(data)

    return data
  }

  async cancelSubscription(
    subscriptionToken: string,
  ): Promise<SubscriptionInfo> {
    const {eppApiKey} = this.config

    await this.eppProxy.cancelSubscription({eppApiKey, subscriptionToken})

    return this.fetchSubscription(subscriptionToken)
  }

  async fetchCustomer(customerToken: string): Promise<CustomerInfo> {
    const {eppApiKey} = this.config

    return this.eppProxy.fetchCustomer({eppApiKey, customerToken})
  }

  async updateCustomer(
    customerToken: string,
    fields: {[key: string]: string | number | boolean},
  ): Promise<CustomerInfo> {
    const {eppApiKey} = this.config

    await this.eppProxy.updateCustomer({eppApiKey, customerToken, fields})

    return this.fetchCustomer(customerToken)
  }

  async fetchReferenceNumber(campaignId?: string): Promise<string | undefined> {
    this.config.debug && console.debug('[epik] fetching refno')
    const {eppApiKey, eppMerchantId} = this.config
    let refno

    try {
      await pRetry(
        async () => {
          try {
            refno = await this.eppProxy.fetchReferenceNumber({
              eppApiKey,
              eppMerchantId,
              campaignId,
            })
          } catch (error) {
            // Rethrow only in case of timeout errors and ignore all other errors.
            // This way only the calls with timeout will be retried.
            if (error.response?.status === 408) {
              throw error
            }
          }
        },
        {
          retries: 5,
          factor: 1,
          minTimeout: 0,
          onFailedAttempt: (error) => {
            this.config.debug &&
              console.log(
                `[epik] fetching refno attempt ${error.attemptNumber} failed`,
              )
          },
        },
      )
    } catch (error) {
      // Do nothing if all retries are failed.
    }

    return refno
  }

  /////////////////////////////////////////////////////////////////////////////
  // epms
  /////////////////////////////////////////////////////////////////////////////

  async fetchEpmsPayment(epmsPaymentUuid: string): Promise<EpmsPaymentInfo> {
    return this.epmsProxy.fetchPayment({epmsPaymentUuid})
  }

  async fetchEpmsPaymentStatus(
    epmsPaymentUuid: string,
  ): Promise<EpmsPaymentStatusInfo> {
    return this.epmsProxy.fetchPaymentStatus({epmsPaymentUuid})
  }

  async fetchEpmsSubscription(
    epmsSubscriptionUuid: string,
  ): Promise<EpmsSubscriptionInfo> {
    return this.epmsProxy.fetchSubscription({epmsSubscriptionUuid})
  }

  async fetchEpmsPaymentSource(
    epmsPaymentSourceUuid: string,
  ): Promise<EpmsPaymentSourceInfo> {
    return this.epmsProxy.fetchPaymentSource({epmsPaymentSourceUuid})
  }

  async fetchEpmsSupporter(
    epmsSupporterUuid: string,
  ): Promise<EpmsSupporterInfo> {
    return this.epmsProxy.fetchSupporter({epmsSupporterUuid})
  }

  async cancelEpmsPayment(epmsPaymentUuid: string) {
    return this.epmsProxy.cancelPayment({epmsPaymentUuid})
  }

  async cancelEpmsSubscription(epmsSubscriptionUuid: string): Promise<void> {
    return this.epmsProxy.cancelSubscription({epmsSubscriptionUuid})
  }

  /////////////////////////////////////////////////////////////////////////////

  private createPaymentFlow(payment: PaymentConfig): IPaymentFlow {
    const paymentFlow = payment.flow

    delete payment.flow

    return createPaymentFlow(paymentFlow)
  }

  /////////////////////////////////////////////////////////////////////////////

  private prepareTransactionInfo(data: TransactionInfo): TransactionInfo {
    data = this.normalizedCurrency<TransactionInfo>(data)
    data = this.normalizedPaymentMethod<TransactionInfo>(data)
    data = this.normalizedFinalStatus(data)

    return data
  }

  private prepareSubscriptionInfo(data: SubscriptionInfo): SubscriptionInfo {
    data = this.normalizedCurrency<SubscriptionInfo>(data)
    data = this.normalizedPaymentMethod<SubscriptionInfo>(data)
    data = this.normalizeRecurringInterval<SubscriptionInfo>(data)

    return data
  }

  /////////////////////////////////////////////////////////////////////////////

  private normalizedCurrency<T extends {currency: any}>(data: T): T {
    data = cloneDeep(data)

    data.currency = (data.currency || '').toLowerCase() as PaymentCurrency

    return data
  }

  private normalizedPaymentMethod<T extends {payment_method: any}>(data: T): T {
    data = cloneDeep(data)

    data.payment_method = (
      data.payment_method || ''
    ).toLowerCase() as PaymentMethod

    return data
  }

  private normalizeRecurringInterval<T extends SubscriptionInfo>(data: T): T {
    data = cloneDeep(data)

    if (!data.stored_rnw_recurring_interval_name) {
      const recurringIntervalName = this.cronHelper.getRecurringIntervalName(
        data.interval,
      )
      data.stored_rnw_recurring_interval_name = (
        recurringIntervalName || ''
      ).toLowerCase()
    }

    return data
  }

  private normalizedFinalStatus(data: TransactionInfo): TransactionInfo {
    data = cloneDeep(data)

    const finalStatusesMap = {
      success: TransactionStatus.SUCCESS,
      aborted_by_user: TransactionStatus.CANCEL,
      final_success: TransactionStatus.SUCCESS,
      error_from_paymentprovider: TransactionStatus.ERROR,
      final_error: TransactionStatus.ERROR,
      final_cancel: TransactionStatus.CANCEL,
      authorization_success: TransactionStatus.SUCCESS,
      success_from_payment_provider: TransactionStatus.SUCCESS,
      timeout: TransactionStatus.CANCEL,
    }

    if (isArray(data.status)) {
      const statusesCount = data.status.length
      const finalStatus = (
        data.status[statusesCount - 1] || ({} as DetailedTransactionStatus)
      ).statusName
      const previousStatus = (
        data.status[statusesCount - 2] || ({} as DetailedTransactionStatus)
      ).statusName

      if (isPaymentMethodWithDeferredStatus(data.payment_method)) {
        // for sms payment in test_mode – we force polling to run at least transactionStatusPollForceRequestsCount times
        const isSmsPaymentMethod = data.payment_method === PaymentMethod.SMS
        const isCounterFinished =
          this.eppPaymentStatusPoll.counter >=
          this.config.paymentStatusPollForceRequestsCount
        const isPollForced =
          data.test_mode && isSmsPaymentMethod && !isCounterFinished

        if (isPollForced) {
          data.epayment_status = TransactionStatus.PENDING
        } else {
          // default epayment_status is "pending"
          data.epayment_status = TransactionStatus.PENDING

          if (
            finalStatus === 'final_error' ||
            finalStatus === 'aborted_by_user' ||
            finalStatus === 'final_cancel'
          ) {
            data.epayment_status = TransactionStatus.CANCEL
          }

          if (finalStatus === 'final_success') {
            data.epayment_status = TransactionStatus.SUCCESS
          }
        }
      } else {
        const isSodPaymentMethod =
          data.payment_method === PaymentMethod.SEPA_ONE
        const isRecurring =
          !!data.recurring_interval && !!data.subscription_token

        // determine status from mapping
        data.epayment_status =
          finalStatusesMap[finalStatus] || TransactionStatus.ERROR

        // handle special cases
        if (
          finalStatus === 'final_cancel' &&
          previousStatus === 'authorization_success'
        ) {
          data.epayment_status = TransactionStatus.SUCCESS
        }

        if (
          isSodPaymentMethod &&
          isRecurring &&
          finalStatus === 'final_cancel' &&
          previousStatus === 'success'
        ) {
          data.epayment_status = TransactionStatus.SUCCESS
        }
      }
    } else {
      data.epayment_status = TransactionStatus.ERROR
    }

    return data
  }

  /////////////////////////////////////////////////////////////////////////////

  private tryStartEppPaymentStatusPoll(args: {
    paymentId: string
    paymentStatus: TransactionStatus
  }) {
    const {paymentId, paymentStatus} = args
    const isPending = paymentStatus === TransactionStatus.PENDING
    const isPaymentStatusPollRunning = this.eppPaymentStatusPoll.isRunning()

    if (paymentId && isPending && !isPaymentStatusPollRunning) {
      this.eppPaymentStatusPoll.start(paymentId)
    }
  }

  private tryStopEppPaymentStatusPoll(paymentStatus: TransactionStatus) {
    const isPending = paymentStatus === TransactionStatus.PENDING
    const isPaymentStatusPollRunning = this.eppPaymentStatusPoll.isRunning()

    if (!isPending && isPaymentStatusPollRunning) {
      this.eppPaymentStatusPoll.stop()
    }
  }

  /////////////////////////////////////////////////////////////////////////////

  private tryStartEpmsPaymentStatusPoll(args: {
    paymentId: string
    paymentStatus: EpmsPaymentStatus
  }) {
    const {paymentId, paymentStatus} = args
    const isPending = paymentStatus === EpmsPaymentStatus.PENDING
    const isPaymentStatusPollRunning = this.epmsPaymentStatusPoll.isRunning()

    if (paymentId && isPending && !isPaymentStatusPollRunning) {
      this.epmsPaymentStatusPoll.start(paymentId)
    }
  }

  private tryStopEpmsPaymentStatusPoll(paymentStatus: EpmsPaymentStatus) {
    const isPending = paymentStatus === EpmsPaymentStatus.PENDING
    const isPaymentStatusPollRunning = this.epmsPaymentStatusPoll.isRunning()

    if (!isPending && isPaymentStatusPollRunning) {
      this.epmsPaymentStatusPoll.stop()
    }
  }
}

///////////////////////////////////////////////////////////////////////////////

export const createEpik = (config: EpikConfig = {}): Epik => {
  config = createEpikConfig(config)

  return new Epik({config})
}
