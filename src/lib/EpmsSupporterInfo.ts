export type EpmsSupporterInfo = {
  uuid: string
  salutation: string
  first_name: string
  last_name: string
  raw_name: string
  email: string
  email_permission: boolean
  street: string
  house_number: string
  address_addendum: string
  postal_code: string
  city: string
  country: string
  region_level_1: string
  raw_address: string

  metadata: {
    [key: string]: any
  }
}
