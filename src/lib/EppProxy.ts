import axios, {AxiosPromise, AxiosRequestConfig} from 'axios'
import {Epik} from './Epik'
import {CustomerInfo} from './CustomerInfo'
import {TransactionInfo} from './TransactionInfo'
import {SubscriptionInfo} from './SubscriptionInfo'
import {normalizeValues} from './helpers/values'

///////////////////////////////////////////////////////////////////////////////

export type CreateEppApiProxyArgs = {
  epik: Epik
}

export type FetchTransactionArgs = {
  eppApiKey: string
  eppTransactionId: string
}

export type FetchSubscriptionArgs = {
  eppApiKey: string
  subscriptionToken: string
}

export type CancelSubscriptionArgs = {
  eppApiKey: string
  subscriptionToken: string
}

export type FetchCustomerArgs = {
  eppApiKey: string
  customerToken: string
}

export type UpdateCustomerArgs = {
  eppApiKey: string
  customerToken: string
  fields: {
    [key: string]: string | number | boolean
  }
}

export type FetchReferenceNumberArgs = {
  eppApiKey: string
  eppMerchantId: string
  campaignId?: string
}

export interface IEppApiProxy {
  fetchTransaction(args: FetchTransactionArgs): Promise<TransactionInfo>
  fetchSubscription(args: FetchSubscriptionArgs): Promise<SubscriptionInfo>
  fetchCustomer(args: FetchCustomerArgs): Promise<CustomerInfo>

  cancelSubscription(args: CancelSubscriptionArgs)
  updateCustomer(args: UpdateCustomerArgs)
  fetchReferenceNumber(args: FetchReferenceNumberArgs): Promise<string>
}

///////////////////////////////////////////////////////////////////////////////

export class EppApiProxy implements IEppApiProxy {
  private epik: Epik

  constructor(args: CreateEppApiProxyArgs) {
    const {epik} = args
    this.epik = epik
  }

  async fetchTransaction(args: FetchTransactionArgs): Promise<TransactionInfo> {
    const {eppApiKey, eppTransactionId} = args
    let {data} = await this.sendRequest<TransactionInfo>({
      method: 'get',
      url: `/organisations/${eppApiKey}/transactions/${eppTransactionId}`,
    })
    data = normalizeValues(data)

    return data
  }

  async fetchSubscription(
    args: FetchSubscriptionArgs,
  ): Promise<SubscriptionInfo> {
    const {eppApiKey, subscriptionToken} = args
    let {data} = await this.sendRequest<SubscriptionInfo>({
      method: 'get',
      url: `/organisations/${eppApiKey}/subscriptions/${subscriptionToken}`,
    })
    data = normalizeValues(data)

    return data
  }

  async fetchCustomer(args: FetchCustomerArgs): Promise<CustomerInfo> {
    const {eppApiKey, customerToken} = args
    let {data} = await this.sendRequest<CustomerInfo>({
      method: 'get',
      url: `/organisations/${eppApiKey}/customers/${customerToken}`,
    })
    data = normalizeValues(data)

    return data
  }

  async cancelSubscription(args: CancelSubscriptionArgs) {
    const {eppApiKey, subscriptionToken} = args
    let {data} = await this.sendRequest<any>({
      method: 'post',
      url: `/organisations/${eppApiKey}/subscriptions/${subscriptionToken}/cancel`,
    })
    data = normalizeValues(data)

    return data
  }

  async updateCustomer(args: UpdateCustomerArgs) {
    const {eppApiKey, customerToken, fields} = args
    let {data} = await this.sendRequest<CustomerInfo>({
      method: 'post',
      url: `/organisations/${eppApiKey}/customers/${customerToken}`,
      data: fields,
    })
    data = normalizeValues(data)

    return data
  }

  async fetchReferenceNumber(args: FetchReferenceNumberArgs): Promise<string> {
    const {eppApiKey, eppMerchantId = '', campaignId} = args
    const url = campaignId
      ? `/organisations/${eppApiKey}/payment-slips/${campaignId}`
      : `/organisations/${eppApiKey}/payment-slips`

    const {data} = await this.sendRequest<{refno: string}>({
      method: 'get',
      url,
      params: {
        merchant_id: eppMerchantId,
      },
    })
    const {refno} = normalizeValues(data)

    return refno
  }

  private sendRequest<T = any>(config: AxiosRequestConfig): AxiosPromise<T> {
    const baseUrl = this.epik.config.eppProxyUrl
    const url = baseUrl + config.url

    return axios.request<T>({...config, url})
  }
}

///////////////////////////////////////////////////////////////////////////////

export const createEppApiProxy = (args: CreateEppApiProxyArgs) => {
  return new EppApiProxy(args)
}
