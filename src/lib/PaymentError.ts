// prettier-ignore
export const PaymentError = {
  SUCCESS_URL_MISSING: {code: 'success_url_missing', message: 'Missing success url'},
  ERROR_URL_MISSING: {code: 'error_url_missing', message: 'Missing error url'},
  CANCEL_URL_MISSING: {code: 'cancel_url_missing', message: 'Missing cancel url'},
  TEST_MODE_MISSING: {code: 'test_mode_missing', message: 'Missing test mode setting'},
  TEST_MODE_INVALID: {code: 'test_mode_invalid', message: 'Invalid value'},
  LANGUAGE_MISSING: {code: 'language_missing', message: 'Missing language setting'},
  REQTYPE_MISSING: {code: 'reqtype_missing', message: 'Missing reqtype setting'},
  REQTYPE_INVALID: {code: 'reqtype_invalid', message: 'Invalid value'},
  FURTHER_RNW_INTERACTION_MISSING: {code: 'further_rnw_interaction_missing', message: 'Missing further interaction setting'},
  FURTHER_RNW_INTERACTION_INVALID: {code: 'further_rnw_interaction_invalid', message: 'Invalid value'},
  PAYMENT_METHOD_MISSING: {code: 'payment_method_missing', message: 'Payment method missing'},
  PAYMENT_METHOD_INVALID: {code: 'payment_method_invalid', message: 'Payment method is not supported'},
  PAYMENT_PROVIDER_INVALID: {code: 'payment_provider_invalid', message: 'Payment provider is not supported'},
  RECURRING_INTERVAL_MISSING: {code: 'recurring_interval_missing', message: 'Recurring interval missing'},
  AMOUNT_MISSING: {code: 'amount_missing', message: 'Amount missing'},
  AMOUNT_INVALID: {code: 'amount_invalid', message: 'Invalid value'},
  CURRENCY_MISSING: {code: 'currency_missing', message: 'Missing currency'},
  CARD_HOLDER_NAME_MISSING: {code: 'card_holder_name_missing', message: 'Missing credit card holder name'},
  CARDNO_MISSING: {code: 'cardno_missing', message: 'Missing credit card number'},
  CARDNO_INVALID: {code: 'cardno_invalid', message: 'Invalid credit card number'},
  CVV_MISSING: {code: 'cvv_missing', message: 'Missing credit card cvv'},
  CVV_INVALID: {code: 'cvv_invalid', message: 'Invalid credit card cvv'},
  EXPM_MISSING: {code: 'expm_missing', message: 'Missing credit card expiration month'},
  EXPM_INVALID: {code: 'expm_invalid', message: 'Invalid credit card expiration month'},
  EXPY_MISSING: {code: 'expy_missing', message: 'Missing credit card expiration year'},
  EXPY_INVALID: {code: 'expy_invalid', message: 'Invalid credit card expiration year'},
  STORED_CUSTOMER_FIRSTNAME_MISSING: {code: 'stored_customer_firstname_missing', message: 'Missing customer first name'},
  STORED_CUSTOMER_LASTNAME_MISSING: {code: 'stored_customer_lastname_missing', message: 'Missing customer last name'},
  STORED_CUSTOMER_EMAIL_MISSING: {code: 'stored_customer_email_missing', message: 'Missing email address'},
  STORED_CUSTOMER_STREET_MISSING: {code: 'stored_customer_street_missing', message: 'Missing customer street'},
  STORED_CUSTOMER_ZIP_CODE_MISSING: {code: 'stored_customer_zip_code_missing', message: 'Missing customer zip code'},
  STORED_CUSTOMER_CITY_MISSING: {code: 'stored_customer_city_missing', message: 'Missing customer city'},
  STORED_CUSTOMER_COUNTRY_MISSING: {code: 'stored_customer_country_missing', message: 'Missing customer country'},
  IBAN_MISSING: {code: 'iban_missing', message: 'Missing bank IBAN'},
  IBAN_INVALID: {code: 'iban_invalid', message: 'Invalid bank IBAN'},
  IBAN_UNSUPPORTED: {code: 'iban_unsupported', message: 'This IBAN cannot be used'},
  BIC_MISSING: {code: 'bic_missing', message: 'Missing BIC'},
  BIC_INVALID: {code: 'bic_invalid', message: 'Invalid BIC'},
  BIC_IBAN_NOT_MATCHING: {code: 'bic_iban_not_matching', message: 'Invalid BIC for provided IBAN'},
  BANK_NAME_MISSING: {code: 'bank_name_missing', message: 'Missing bank name'},
  BANK_ZIP_CODE_MISSING: {code: 'bank_zip_code_missing', message: 'Missing bank zip code'},
  BANK_CITY_MISSING: {code: 'bank_city_missing', message: 'Missing bank city'},
  STORED_ES_ORDERED_MISSING: {code: 'stored_es_ordered_missing', message: 'Missing stored_es_ordered'},
  STORED_EZS_ORDERED_MISSING: {code: 'stored_ezs_ordered_missing', message: 'Missing stored_ezs_ordered'},
  STORED_CHQR_ORDERED_MISSING: {code: 'stored_chqr_ordered_missing', message: 'Missing stored_chqr_ordered'},
  REFNO_MISSING: {code: 'refno_missing', message: 'Missing refno'},
  MSISDN_MISSING: {code: 'msisdn_missing', message: 'Missing msisdn'},
  MSISDN_INVALID: {code: 'msisdn_invalid', message: 'Invalid msisdn'},
}
