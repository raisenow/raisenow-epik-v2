import {keys} from 'lodash'
import {
  validateCreditCardCvv,
  validateCreditCardExpirationMonth,
  validateCreditCardExpirationYear,
  validateCreditCardNumber,
} from '../validation/creditCard'
import {validateNumerical} from '../validation/generic'
import {getCountryFromIban, validateIBAN} from '../validation/iban'
import {Epik} from './Epik'
import {PaymentConfig} from './PaymentConfig'
import {PaymentError} from './PaymentError'
import {addError, PaymentErrors} from './PaymentErrors'
import {
  isCreditCardPaymentMethod,
  isSepaPaymentMethod,
  isSupportedPaymentMethod,
  PaymentMethod,
} from './PaymentMethod'
import {isSupportedPaymentProvider} from './PaymentProvider'
import {validateMsisdn} from '../validation/msisdn'
import {sepaCountries, isBicFieldRequired} from '../validation/sepa'
import {PaymentFlow} from './PaymentFlow'

///////////////////////////////////////////////////////////////////////////////

export class PaymentValidator {
  validateMetadata(payment: PaymentConfig): PaymentErrors {
    const errors = {} as PaymentErrors

    const hasSuccessUrl = !!payment.success_url
    const hasErrorUrl = !!payment.error_url
    const hasCancelUrl = !!payment.cancel_url
    const hasTestMode = keys(payment).includes('test_mode')
    const isValidTestMode = [true, false].includes(payment.test_mode)
    const hasLanguage = !!payment.language
    const hasReqtype = !!payment.reqtype
    const isValidReqtype = payment.reqtype === 'CAA'
    const hasFurtherRnwInteraction = !!payment.further_rnw_interaction
    const isValidFurtherRnwInteraction = ['enabled', 'disabled'].includes(
      payment.further_rnw_interaction as string,
    )

    if (!hasSuccessUrl) {
      addError(errors, 'success_url', PaymentError.SUCCESS_URL_MISSING)
    }

    if (!hasErrorUrl) {
      addError(errors, 'error_url', PaymentError.ERROR_URL_MISSING)
    }

    if (!hasCancelUrl) {
      addError(errors, 'cancel_url', PaymentError.CANCEL_URL_MISSING)
    }

    if (!hasTestMode) {
      addError(errors, 'test_mode', PaymentError.TEST_MODE_MISSING)
    } else if (!isValidTestMode) {
      addError(errors, 'test_mode', PaymentError.TEST_MODE_INVALID)
    }

    if (!hasLanguage) {
      addError(errors, 'language', PaymentError.LANGUAGE_MISSING)
    }

    if (!hasReqtype) {
      addError(errors, 'reqtype', PaymentError.REQTYPE_MISSING)
    } else if (!isValidReqtype) {
      addError(errors, 'reqtype', PaymentError.REQTYPE_INVALID)
    }

    if (!hasFurtherRnwInteraction) {
      addError(
        errors,
        'further_rnw_interaction',
        PaymentError.FURTHER_RNW_INTERACTION_MISSING,
      )
    } else if (!isValidFurtherRnwInteraction) {
      addError(
        errors,
        'further_rnw_interaction',
        PaymentError.FURTHER_RNW_INTERACTION_INVALID,
      )
    }

    return errors
  }

  validatePaymentMethod(payment: PaymentConfig): PaymentErrors {
    const errors = {} as PaymentErrors

    const hasPaymentMethod = !!payment.payment_method
    const isValidPaymentMethod = isSupportedPaymentMethod(
      payment.payment_method,
    )

    if (!hasPaymentMethod) {
      addError(errors, 'payment_method', PaymentError.PAYMENT_METHOD_MISSING)
    } else if (!isValidPaymentMethod) {
      addError(errors, 'payment_method', PaymentError.PAYMENT_METHOD_INVALID)
    }

    return errors
  }

  validatePaymentProvider(payment: PaymentConfig): PaymentErrors {
    const errors = {} as PaymentErrors

    const hasPaymentProvider = !!payment.payment_provider
    const isValidPaymentProvider = isSupportedPaymentProvider(
      payment.payment_provider,
    )

    if (hasPaymentProvider && !isValidPaymentProvider) {
      addError(
        errors,
        'payment_provider',
        PaymentError.PAYMENT_PROVIDER_INVALID,
      )
    }

    return errors
  }

  validateRecurring(payment: PaymentConfig): PaymentErrors {
    const errors = {} as PaymentErrors

    const isRecurring = !!payment.recurring
    const isPseudoRecurring = !!payment.stored_pseudo_recurring

    if (isRecurring || isPseudoRecurring) {
      const hasRecurringInterval = !!payment.recurring_interval
      const hasEmail = !!payment.stored_customer_email

      if (!hasRecurringInterval) {
        addError(
          errors,
          'recurring_interval',
          PaymentError.RECURRING_INTERVAL_MISSING,
        )
      }

      if (!hasEmail) {
        addError(
          errors,
          'stored_customer_email',
          PaymentError.STORED_CUSTOMER_EMAIL_MISSING,
        )
      }
    }

    return errors
  }

  validateAmount(payment: PaymentConfig): PaymentErrors {
    const errors = {} as PaymentErrors

    const hasAmount =
      !!payment.amount && payment.amount !== payment.stored_rnw_cover_fee_amount
    const isValidAmount = validateNumerical(payment.amount)

    if (!hasAmount) {
      addError(errors, 'amount', PaymentError.AMOUNT_MISSING)
    } else if (!isValidAmount) {
      addError(errors, 'amount', PaymentError.AMOUNT_INVALID)
    }

    return errors
  }

  validateCurrency(payment: PaymentConfig): PaymentErrors {
    const errors = {} as PaymentErrors

    const hasCurrency = !!payment.currency

    if (!hasCurrency) {
      addError(errors, 'currency', PaymentError.CURRENCY_MISSING)
    }

    return errors
  }

  validateCreditCard(payment: PaymentConfig, epik: Epik): PaymentErrors {
    const errors = {} as PaymentErrors

    if (isCreditCardPaymentMethod(payment.payment_method)) {
      const hasCardHolderName = !!payment.card_holder_name

      if (!hasCardHolderName) {
        addError(
          errors,
          'card_holder_name',
          PaymentError.CARD_HOLDER_NAME_MISSING,
        )
      }

      const isEpp = payment.flow === PaymentFlow.EPP
      const hasExpm = !!payment.expm
      const hasExpy = !!payment.expy
      const isValidExpm = validateCreditCardExpirationMonth(payment.expm)
      const isValidExpy = validateCreditCardExpirationYear(payment.expy)

      // We use PCI Proxy SecureFields for EPMS,
      // so we don't have "cardno" and "cvv" fields in the payment object
      if (isEpp) {
        const skipCvv = !!epik.config.skipCvv
        const hasCardNumber = !!payment.cardno
        const hasCvv = !!payment.cvv
        const isValidCardNumber = validateCreditCardNumber(payment.cardno)
        const isValidCvv = validateCreditCardCvv(payment.cvv)

        if (!hasCardNumber) {
          addError(errors, 'cardno', PaymentError.CARDNO_MISSING)
        } else if (!isValidCardNumber) {
          addError(errors, 'cardno', PaymentError.CARDNO_INVALID)
        }

        if (!skipCvv) {
          if (!hasCvv) {
            addError(errors, 'cvv', PaymentError.CVV_MISSING)
          } else if (!isValidCvv) {
            addError(errors, 'cvv', PaymentError.CVV_INVALID)
          }
        }
      }

      if (!hasExpm) {
        addError(errors, 'expm', PaymentError.EXPM_MISSING)
      } else if (!isValidExpm) {
        addError(errors, 'expm', PaymentError.EXPM_INVALID)
      }

      if (!hasExpy) {
        addError(errors, 'expy', PaymentError.EXPY_MISSING)
      } else if (!isValidExpy) {
        addError(errors, 'expy', PaymentError.EXPY_INVALID)
      }
    }

    return errors
  }

  validateDirectDebit(payment: PaymentConfig): PaymentErrors {
    const errors = {} as PaymentErrors

    if (payment.payment_method === PaymentMethod.DIRECT_DEBIT) {
      const hasIban = !!payment.iban
      const isValidIban = validateIBAN(payment.iban)
      const hasBic = !!payment.bic
      const hasBankName = !!payment.bank_name
      const hasBankZipCode = !!payment.bank_zip_code
      const hasBankCity = !!payment.bank_city

      if (!hasIban) {
        addError(errors, 'iban', PaymentError.IBAN_MISSING)
      } else if (!isValidIban) {
        addError(errors, 'iban', PaymentError.IBAN_INVALID)
      }

      if (!hasBic) {
        addError(errors, 'bic', PaymentError.BIC_MISSING)
      }

      if (!hasBankName) {
        addError(errors, 'bank_name', PaymentError.BANK_NAME_MISSING)
      }

      if (!hasBankZipCode) {
        addError(errors, 'bank_zip_code', PaymentError.BANK_ZIP_CODE_MISSING)
      }

      if (!hasBankCity) {
        addError(errors, 'bank_city', PaymentError.BANK_CITY_MISSING)
      }
    }

    return errors
  }

  validateElv(payment: PaymentConfig): PaymentErrors {
    const errors = {} as PaymentErrors

    if (
      payment.payment_method ===
      PaymentMethod.ELEKTRONISCHES_LASTSCHRIFTVERFAHREN
    ) {
      const hasIban = !!payment.iban
      const isValidIban = validateIBAN(payment.iban)

      if (!hasIban) {
        addError(errors, 'iban', PaymentError.IBAN_MISSING)
      } else if (!isValidIban) {
        addError(errors, 'iban', PaymentError.IBAN_INVALID)
      }
    }

    return errors
  }

  validateSepa(payment: PaymentConfig): PaymentErrors {
    const errors = {} as PaymentErrors
    const {bic, iban, payment_method} = payment

    if (!isSepaPaymentMethod(payment_method)) {
      return errors
    }

    const ibanCountry = getCountryFromIban(iban)

    if (!iban) {
      addError(errors, 'iban', PaymentError.IBAN_MISSING)
    } else if (!validateIBAN(iban)) {
      addError(errors, 'iban', PaymentError.IBAN_INVALID)
    } else if (!sepaCountries.includes(ibanCountry)) {
      addError(errors, 'iban', PaymentError.IBAN_UNSUPPORTED)
    }

    if (
      !bic &&
      (payment_method === PaymentMethod.SEPA_ONE ||
        isBicFieldRequired(ibanCountry))
    ) {
      addError(errors, 'bic', PaymentError.BIC_MISSING)
    } else if (bic) {
      const match = bic.match(/^\w{4}([A-Z]{2})\w{2,5}$/)

      if (!match) {
        addError(errors, 'bic', PaymentError.BIC_INVALID)
      } else {
        if (ibanCountry && ibanCountry !== match[1]) {
          addError(errors, 'bic', PaymentError.BIC_IBAN_NOT_MATCHING)
        }
      }
    }

    return errors
  }

  validatePaymentSlip(payment: PaymentConfig, epik: Epik): PaymentErrors {
    const errors = {} as PaymentErrors

    const isEs = payment.payment_method === PaymentMethod.PAYMENT_SLIP
    const isEzs =
      payment.payment_method ===
      PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER
    const isChqr = payment.payment_method === PaymentMethod.SWISS_QR_BILL
    const hasRefno = !!payment.refno
    const hasEsOrdered = keys(payment).includes('stored_es_ordered')
    const hasEzsOrdered = keys(payment).includes('stored_ezs_ordered')
    const hasChqrOrdered = keys(payment).includes('stored_chqr_ordered')

    if (isEs && !hasEsOrdered) {
      addError(
        errors,
        'stored_es_ordered',
        PaymentError.STORED_ES_ORDERED_MISSING,
      )
    }

    if (isEzs && !hasEzsOrdered) {
      addError(
        errors,
        'stored_ezs_ordered',
        PaymentError.STORED_EZS_ORDERED_MISSING,
      )
    }

    if (isChqr && !hasChqrOrdered) {
      addError(
        errors,
        'stored_chqr_ordered',
        PaymentError.STORED_CHQR_ORDERED_MISSING,
      )
    }

    if (isEzs && !hasRefno) {
      addError(errors, 'refno', PaymentError.REFNO_MISSING)
    }

    if (isChqr && !hasRefno && epik.config.requireSwissQrBillReferenceNumber) {
      addError(errors, 'refno', PaymentError.REFNO_MISSING)
    }

    return errors
  }

  validateSms(payment: PaymentConfig): PaymentErrors {
    const errors = {} as PaymentErrors

    if (payment.payment_method === PaymentMethod.SMS) {
      const hasMsisdn = !!payment.msisdn
      const isValidMsisdn = validateMsisdn(payment.msisdn)

      if (!hasMsisdn) {
        addError(errors, 'msisdn', PaymentError.MSISDN_MISSING)
      } else if (!isValidMsisdn) {
        addError(errors, 'msisdn', PaymentError.MSISDN_INVALID)
      }
    }

    return errors
  }

  validateCustomer(payment: PaymentConfig): PaymentErrors {
    const errors = {} as PaymentErrors

    const paymentMethods = [
      PaymentMethod.DIRECT_DEBIT,
      PaymentMethod.PAYMENT_SLIP,
      PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER,
      PaymentMethod.SWISS_QR_BILL,
    ]

    const validateCustomer =
      paymentMethods.includes(payment.payment_method) ||
      isCreditCardPaymentMethod(payment.payment_method)

    if (validateCustomer) {
      const hasFirstName = !!payment.stored_customer_firstname
      const hasLastName = !!payment.stored_customer_lastname

      if (!hasFirstName) {
        addError(
          errors,
          'stored_customer_firstname',
          PaymentError.STORED_CUSTOMER_FIRSTNAME_MISSING,
        )
      }

      if (!hasLastName) {
        addError(
          errors,
          'stored_customer_lastname',
          PaymentError.STORED_CUSTOMER_LASTNAME_MISSING,
        )
      }
    }

    return errors
  }

  validateCustomerAddress(payment: PaymentConfig): PaymentErrors {
    const errors = {} as PaymentErrors

    const paymentMethods = [
      PaymentMethod.DIRECT_DEBIT,
      PaymentMethod.PAYMENT_SLIP,
      PaymentMethod.PAYMENT_SLIP_WITH_REFERENCE_NUMBER,
      PaymentMethod.SWISS_QR_BILL,
    ]

    const validateAddress =
      paymentMethods.includes(payment.payment_method) ||
      isCreditCardPaymentMethod(payment.payment_method)

    if (validateAddress) {
      const hasStreet = !!payment.stored_customer_street
      const hasZipCode = !!payment.stored_customer_zip_code
      const hasCity = !!payment.stored_customer_city
      const hasCountry = !!payment.stored_customer_country

      if (!hasStreet) {
        addError(
          errors,
          'stored_customer_street',
          PaymentError.STORED_CUSTOMER_STREET_MISSING,
        )
      }

      if (!hasZipCode) {
        addError(
          errors,
          'stored_customer_zip_code',
          PaymentError.STORED_CUSTOMER_ZIP_CODE_MISSING,
        )
      }

      if (!hasCity) {
        addError(
          errors,
          'stored_customer_city',
          PaymentError.STORED_CUSTOMER_CITY_MISSING,
        )
      }

      if (!hasCountry) {
        addError(
          errors,
          'stored_customer_country',
          PaymentError.STORED_CUSTOMER_COUNTRY_MISSING,
        )
      }
    }

    return errors
  }
}

///////////////////////////////////////////////////////////////////////////////

export const createPaymentValidator = (): PaymentValidator => {
  return new PaymentValidator()
}
