///////////////////////////////////////////////////////////////////////////////
// Import polyfills
///////////////////////////////////////////////////////////////////////////////
import './router/locationchange'

///////////////////////////////////////////////////////////////////////////////
// Re-export types
///////////////////////////////////////////////////////////////////////////////
export type {CustomerInfo} from './lib/CustomerInfo'
export type {EpikConfig} from './lib/EpikConfig'
export type {EpikEvent, EpikEvents, EpikEventHandler} from './lib/EpikEventer'
export type {
  EpmsPaymentStatusHistoryEntry,
  EpmsPaymentMetadataEntry,
} from './lib/EpmsPaymentInfoBase'
export type {EpmsPaymentInfo} from './lib/EpmsPaymentInfo'
export type {EpmsPaymentSourceInfo} from './lib/EpmsPaymentSourceInfo'
export type {
  EpmsSubscriptionInfo,
  EpmsSubscriptionHistoryEntry,
} from './lib/EpmsSubscriptionInfo'
export type {EpmsSupporterInfo} from './lib/EpmsSupporterInfo'
export type {
  PaymentRequestData,
  PaymentResponseData,
} from './lib/PaymentFlowEpms'
export type {PaymentConfig, EpmsPaymentConfig} from './lib/PaymentConfig'
export type {PaymentErrors, DetailedPaymentError} from './lib/PaymentErrors'
export type {SubscriptionInfo} from './lib/SubscriptionInfo'
export type {
  TransactionInfo,
  DetailedTransactionStatus,
} from './lib/TransactionInfo'
export type {RouterLocation, Router} from './router'

///////////////////////////////////////////////////////////////////////////////
// Re-export enums
///////////////////////////////////////////////////////////////////////////////
export {EpikEventName} from './lib/EpikEventer'
export {EpikQueryParam} from './lib/EpikQueryParam'
export {
  EpmsPaymentStatus,
  EpmsPaymentStatusReason,
} from './lib/EpmsPaymentInfoBase'
export {EpmsSubscriptionStatus} from './lib/EpmsSubscriptionInfo'
export {PaymentCurrency} from './lib/PaymentCurrency'
export {PaymentFlow} from './lib/PaymentFlow'
export {PaymentMethod} from './lib/PaymentMethod'
export {PaymentProvider} from './lib/PaymentProvider'
export {PaymentType} from './lib/PaymentType'
export {RecurringInterval} from './lib/RecurringInterval'
export {SubscriptionStatus} from './lib/SubscriptionInfo'
export {TransactionStatus} from './lib/TransactionInfo'

///////////////////////////////////////////////////////////////////////////////
// Re-export others
///////////////////////////////////////////////////////////////////////////////
export {CronHelper, createCronHelper} from './lib/CronHelper'
export {createEpik, Epik} from './lib/Epik'
export {createEpikConfig} from './lib/EpikConfig'
export {createEpikEvents, EpikEventer} from './lib/EpikEventer'
export {AllEpikQueryParams, WatchedEpikQueryParams} from './lib/EpikQueryParam'
export {createEpmsApiProxy, EpmsApiProxy} from './lib/EpmsProxy'
export {createEppApiProxy, EppApiProxy} from './lib/EppProxy'
export {createPayment, Payment} from './lib/Payment'
export {PaymentError} from './lib/PaymentError'
export {
  SUPPORTER_FIELDS,
  SUPPORTER_METADATA_FIELDS,
  RNW_INTEGRATION_FIELDS,
  RNW_PRODUCT_FIELDS,
} from './lib/PaymentFlowEpms'
export {
  isSupportedPaymentMethod,
  isCreditCardPaymentMethod,
  isPostFinancePaymentMethod,
  isSepaPaymentMethod,
  isTwintPaymentMethod,
  isPaymentMethodWithDeferredStatus,
  getCreditCardType,
  CreditCardPaymentMethods,
  PostFinancePaymentMethods,
  PaymentMethodsWithDeferredStatus,
} from './lib/PaymentMethod'
export {isSupportedPaymentProvider} from './lib/PaymentProvider'
export {
  getSupportedPaymentTypes,
  isPaymentTypeSupported,
  OneTimePaymentMethods,
  RecurringPaymentMethods,
  PseudoRecurringPaymentMethods,
} from './lib/PaymentType'
export {getEpmsPaymentMetadataEntryValue} from './lib/EpmsPaymentInfoBase'
export {
  createPaymentStatusPoll,
  PaymentStatusPoll,
} from './lib/PaymentStatusPoll'
export {normalizeValues} from './lib/helpers/values'
export {prepareReturnUrl} from './lib/helpers/returnUrl'
export {getLocationParams} from './lib/helpers/location'
export {getCountryFromIban} from './validation/iban'
export {isBicFieldRequired} from './validation/sepa'
