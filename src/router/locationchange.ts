;(() => {
  if (window['rnw.locationchangeEventAdded']) {
    return
  }

  window.history.pushState = ((f) => {
    return function () {
      // eslint-disable-next-line prefer-rest-params
      const ret = f.apply(this, arguments)
      window.dispatchEvent(new CustomEvent('rnw.pushstate'))
      window.dispatchEvent(new CustomEvent('rnw.locationchange'))

      return ret
    }
    // eslint-disable-next-line @typescript-eslint/unbound-method
  })(window.history.pushState)

  window.history.replaceState = ((f) => {
    return function () {
      // eslint-disable-next-line prefer-rest-params
      const ret = f.apply(this, arguments)
      window.dispatchEvent(new CustomEvent('rnw.replacestate'))
      window.dispatchEvent(new CustomEvent('rnw.locationchange'))

      return ret
    }
    // eslint-disable-next-line @typescript-eslint/unbound-method
  })(window.history.replaceState)

  window.addEventListener('popstate', () => {
    window.dispatchEvent(new CustomEvent('rnw.locationchange'))
  })

  window['rnw.locationchangeEventAdded'] = true
})()

// https://stackoverflow.com/questions/56577201/why-is-isolatedmodules-error-fixed-by-any-import
export {}
