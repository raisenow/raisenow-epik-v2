export type Router = {
  getHistoryMode(): HistoryMode
  getLocation(): RouterLocation
  push(url): void
  replace(url): void
  listen(locationChangeHandler: LocationChangeHandler): () => void
}

export enum HistoryMode {
  BROWSER = 'browser',
  HASH = 'hash',
}

export type RouterLocation = {
  pathname: string
  search: string
  hash: string
}

export type LocationChangeHandler = (location: RouterLocation) => void

///////////////////////////////////////////////////////////////////////////////

export const createRouter = (
  historyMode: HistoryMode = HistoryMode.BROWSER,
): Router => {
  const getHistoryMode = (): HistoryMode => historyMode

  const getLocation = (): RouterLocation => {
    let url = null

    if (historyMode === HistoryMode.BROWSER) {
      url = window.location.href
    }

    if (historyMode === HistoryMode.HASH) {
      url = window.location.hash.replace(/^#/, '')
    }

    return getLocationFromUrl(url)
  }

  const getLocationFromUrl = (url: string) => {
    if (url === '') {
      return {
        pathname: '/',
        search: '',
        hash: '',
      }
    }

    const a = document.createElement('a')
    a.href = url
    // eslint-disable-next-line prefer-const
    let {pathname, search, hash} = a

    // ie11 doesn't have leading slash
    if (pathname[0] !== '/') {
      pathname = `/${pathname}`
    }

    return {pathname, search, hash}
  }

  const buildFullUrlFromLocation = (location: RouterLocation) => {
    const {pathname, search, hash} = location

    if (historyMode === HistoryMode.BROWSER) {
      return pathname + search + hash
    }

    if (historyMode === HistoryMode.HASH) {
      const hashUrl = pathname + search + hash
      const {pathname: wPathname, search: wSearch} = window.location

      return wPathname + wSearch + '#' + hashUrl
    }
  }

  const push = (rawUrl: string) => {
    const location = getLocationFromUrl(rawUrl)
    const url = buildFullUrlFromLocation(location)
    window.history.pushState(null, null, url)
  }

  const replace = (rawUrl: string) => {
    const location = getLocationFromUrl(rawUrl)
    const url = buildFullUrlFromLocation(location)
    window.history.replaceState(null, null, url)
  }

  const listen = (locationChangeHandler: LocationChangeHandler) => {
    const cb = () => locationChangeHandler(getLocation())
    window.addEventListener('rnw.locationchange', cb)

    return () => window.removeEventListener('rnw.locationchange', cb)
  }

  return {getHistoryMode, getLocation, push, replace, listen}
}
