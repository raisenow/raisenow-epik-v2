module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        ignoreBrowserslistConfig: true,
      },
    ],
    '@babel/preset-typescript',
  ],
  plugins: [
    'transform-inline-environment-variables',
    [
      '@babel/plugin-transform-runtime',
      {
        corejs: {
          version: 3,
          proposals: true,
        },
      },
    ],
    'lodash',
  ],
}
