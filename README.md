# EPIK

E-Payment Integration Kit.

It's a JS-library that allows you to integrate Raisenow's E-payment functionalities into your website.

## Commands

### build

Buld the bundle into _dist_ folder.

```
npm run build
```

### build:watch

Watch file changes and rebuild automatically.

```
npm run build:watch
```

## Sample widget implementation flow

```html
<script src="dist/epik.browser.min.js"></script>
<script>
  //////////////////////////////////////////////////////////////////////////////
  // 1. Create and initialize a new EPIK instance
  //////////////////////////////////////////////////////////////////////////////

  // Create EPIK instance
  epik = rnw.epik.createEpik({
    language: 'en',
    // // default
    // eppApiKey: '1234567890',

    // // chqr (Franco, no reference number is configured)
    // eppApiKey: 'rai-wp5s4u',

    // chqr (Jens, reference number is configured)
    eppApiKey: 'sal-rmz8x0',
    eppMerchantId: 'sal-qfb3eu',
    requireSwissQrBillReferenceNumber: true,

    autoFetchReferenceNumber: true,

    debug: true,
  })

  // Subscribe to events
  epik.events.fetchPaymentDataEnd.subscribe(function (event) {
    var transactionInfo = event.data.transactionInfo

    console.log('--> payment', {
      status: transactionInfo && transactionInfo.epayment_status,
      transactionInfo: transactionInfo,
    })
  })

  // Initialize EPIK instance
  epik.init()

  //////////////////////////////////////////////////////////////////////////////
  // 2. Create payment
  //////////////////////////////////////////////////////////////////////////////

  // // Create a new payment object (cc)
  // payment = epik.createPayment({
  //   flow: 'epp',
  //   payment_method: 'cc',
  //   currency: 'chf',
  //   amount: 100,
  //   cardno: '4242424242424242',
  //   cvv: '123',
  //   expm: '06',
  //   expy: '25',
  //   stored_customer_firstname: 'John',
  //   stored_customer_lastname: 'Snow',
  //   stored_customer_street: 'Hardturmstrasse',
  //   stored_customer_street_number: '101',
  //   stored_customer_zip_code: '8005',
  //   stored_customer_city: 'Zurich',
  //   stored_customer_country: 'CH',
  // })

  // // Create a new payment object (ezs)
  // payment = epik.createPayment({
  //   flow: 'epp',
  //   payment_method: 'ezs',
  //   currency: 'chf',
  //   amount: 100,
  //   stored_customer_firstname: 'John',
  //   stored_customer_lastname: 'Snow',
  //   stored_customer_street: 'Hardturmstrasse',
  //   stored_customer_street_number: '101',
  //   stored_customer_zip_code: '8005',
  //   stored_customer_city: 'Zurich',
  //   stored_customer_country: 'CH',
  //   stored_ezs_ordered: false,
  // })

  // Create a new payment object (chqr)
  payment = epik.createPayment({
    flow: 'epp',
    payment_method: 'chqr',
    currency: 'chf',
    amount: 100,
    stored_customer_firstname: 'John',
    stored_customer_lastname: 'Snow',
    stored_customer_street: 'Hardturmstrasse',
    stored_customer_street_number: '101',
    stored_customer_zip_code: '8005',
    stored_customer_city: 'Zurich',
    stored_customer_country: 'CH',
    stored_chqr_ordered: false,
  })

  updatePayment = function () {
    // payment.update({
    //   cardno: '4242424242424242',
    //   cvv: '123',
    //   expm: '06',
    //   expy: '25',
    // })

    epik.fetchReferenceNumber().then(function (refno) {
      payment.update({
        refno: refno,
      })
    })
  }

  // Define sendPayment function
  sendPayment = function () {
    // Validate payment
    payment.validate().then(function (errors) {
      if (errors) {
        // Handle validation errors if any
        console.log('validation errors:', errors)
      } else {
        // Send payment
        payment.send()
      }
    })
  }

  //////////////////////////////////////////////////////////////////////////////
  // 3. Send payment
  //////////////////////////////////////////////////////////////////////////////

  // Run in console:
  // sendPayment()
</script>
```
