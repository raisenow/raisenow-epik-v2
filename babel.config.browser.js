const excludedModules = [
  'node_modules/@babel/runtime-corejs3',
  'node_modules/core-js-pure',
  'node_modules/regenerator-runtime',
]

module.exports = {
  presets: ['@babel/preset-env', '@babel/preset-typescript'],
  plugins: [
    'transform-inline-environment-variables',
    [
      '@babel/plugin-transform-runtime',
      {
        corejs: {
          version: 3,
          proposals: true,
        },
      },
    ],
    'lodash',
  ],
  exclude: (path) => excludedModules.some((v) => path.includes(v)),
  // exclude: 'node_modules/**',
}
